#!/bin/bash

if ! (return 0 2>/dev/null); then
	source ocdctrl.sh ## Try source (if possible) to get the trampoline
	__ocdctrl_source_trampoline log
	__ocdctrl_source_trampoline misc
	__ocdctrl_source_trampoline getopts-fu
	__ocdctrl_source_ocdctrl
fi
ocdctrl_cmd[template]='user'

function synopsis_template() {
	cat <<-EOF
	template [OPTION]... [COMMAND]...
	EOF
}

function usage_template() {
	cat <<-EOF
	template [-l | [OPTION]... [COMMAND]...]
	EOF
}

function help_template() {
	cat <<-EOF
	NAME
	   ocdctrl-template

	SYNOPSIS
	   $(synopsis_template)

	DESCRIPTION
	   Display template about a command or list of commands

	EXAMPLES
	   template -h
	      Shows help for template

	OPERATION
	   This is ocdctrl-template

	OPTIONS
	   -h                 This help

	ARGUMENTS
	   None or a list of image names

	SEE ALSO
	   Related commands:
	$(__ocdctrl_help -j6 -tu $(awk '{print "-i " $0}' <<< ${ocdctrl_cmd[template]}))

	   Back-end command:
	      $(__ocdctrl_help -j6 -u list)

	AUTHOR
	   Michael Ambrus <michael@helsinova.se>, Sept 2019

	EOF
}

function getopts_template() {
	while getopts $_options OPTION; do
		case $OPTION in
		i)
			_nspaces="$OPTARG"
			;;
		esac
	done
	OPTIND=1

	return $?
}

function __ocdctrl_template() {
	local _nspaces

	_getopts_begin i: help_template
		getopts_template "${@}"
	_getopts_end "${@}"
	shift $(($OPTIND - 1))

	function template_process_one() {
		local _img_name=$1
		LOGI $_img_name
	}

	__ocdctrl_handle_input -a ${OCDCTRL_PROJECT} -f template_process_one -- $@

	return $?
}

if ! (return 0 2>/dev/null); then
	__ocdctrl_template "${@}"
	exit $?
fi
