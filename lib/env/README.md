# Environment

Variables and settings that influence the data definition and operation go
here. This directory contain version specific templates.

If project-version has changed, you should carefully examine and possibly
remove for re-creation the files in this directory.

## Files

### ```version.env```

Project version variables. NEVER change content of this file manually.

### ```constants.env```

Content in this file is to be regarded as hard-coded. Each variables
typically affect BOTH data-definition and operation. I.e. changes here
should not be taken lightly.

It's the last environment file to be loaded and as such each variable
mentioned here will have presidency over any other environment source,
including options, manual setting of variable e.t.c.

### ```settings.env```

Softer variables that can, and should, be changed for particular setup.
