#!/bin/bash

if ! (return 0 2>/dev/null); then
	source ocdctrl.sh ## Try source (if possible) to get the trampoline
	__ocdctrl_source_trampoline log
	__ocdctrl_source_trampoline misc
	__ocdctrl_source_trampoline getopts-fu
	__ocdctrl_source_ocdctrl
fi
ocdctrl_cmd[gdbserver]='user'

function synopsis_gdbserver() {
	cat <<-EOF
	gdbserver [OPTION]... [COMMAND]...
	EOF
}

function usage_gdbserver() {
	cat <<-EOF
	gdbserver [-l | [OPTION]... [COMMAND]...]
	EOF
}

function help_gdbserver() {
	cat <<-EOF
	NAME
	   ocdctrl-gdbserver

	SYNOPSIS
	   $(synopsis_gdbserver)

	DESCRIPTION
	   ocdctrl gdbserver service front-end

	EXAMPLES
	   gdbserver -h
	      Shows help for gdbserver

	   gdbserver -i
	      server running or not

	OPERATION
	   ocdctrl-gdbserver is used to operate on the service. I.e.
	   start/stop/inspect.

	   A service is run in background using gnu-screen. One can attach either
	   directly to the terminal multiplexer session or using OpenOCD port
	   (default). The former is for debugging this service and is not
	   preferred.

	   ocdctrl-gdbserver doesn't accept options for selecting interface,
	   target or transport method to release complexity of usage. As there
	   may be many services running using different combinations for different
	   projects. Instead these are kept as variables managed by this
	   frame-works configure and environment managing.

	   Influential environment variables:
	   ----------------------------------
	      Normally set in project-profile using ocdctrl -p <project> env -e

	      OCDCTRL_CFG_INTERFACE: interface configuration file [$OCDCTRL_CFG_INTERFACE]
	      OCDCTRL_CFG_TARGET   : target configuration file [$OCDCTRL_CFG_TARGET]
	      OCDCTRL_TRANSPORT    : transport protocol [$OCDCTRL_TRANSPORT]
	      OCDCTRL_PORT_GDB     : gdbserver-port [$OCDCTRL_PORT_GDB]
	      OCDCTRL_PORT_TELNET  : telnet-port [$OCDCTRL_PORT_TELNET]
	      OCDCTRL_PORT_TCL     : tcl-port [$OCDCTRL_PORT_TCL]

	OPTIONS
	   -h                   This help
	   -i                   Show server status
	   -a                   List all servers running
	   -A                   Attach directly when attaching

	ARGUMENTS
	   Sub-command, default [start]

	   start - starts server
	   stop  - stops server
	   attach - attach to server

	SEE ALSO
	   Related commands:
	$(__ocdctrl_help -j6 -tu $(awk '{print "-i " $0}' <<< ${ocdctrl_cmd[gdbserver]}))

	   Back-end command:
	      $(__ocdctrl_help -j6 -u list)

	AUTHOR
	   Michael Ambrus <michael@helsinova.se>, February 2020

	EOF
}

function getopts_gdbserver() {
	while getopts $_options OPTION; do
		case $OPTION in
		i)
			_show_status='yes'
			;;
		a)
			_list_all='yes'
			;;
		A)
			_direct_attach='yes'
			;;
		esac
	done
	OPTIND=1

	return $?
}

function __ocdctrl_gdbserver() {
	local _direct_attach
	local _show_status
	local _list_all
	local _cmd
	local -a _subcommands=(
		"start"
		"stop"
		"attach"
	)
	local _session_name="gdbserver_${OCDCTRL_PROJECT}"

	_getopts_begin iaA help_gdbserver
		getopts_gdbserver "${@}"
	_getopts_end "${@}"
	shift $(($OPTIND - 1))

	function service_status() {
		if [ "X$(screen -ls | grep $_session_name)" == "X" ]; then
			echo "Stopped"
			return 1
		else
			echo "Running"
			return 0
		fi
	}

	function start() {
		local _transport=$(tr '[:upper:]' '[:lower:]' <<< "${OCDCTRL_TRANSPORT}")
		local _status 	## Local declaration and assignment needs separated to
							## capture exit-code.

		# Generate a list of commands that go in-between reading interface
		# and reading target. Port commands exluded
		function cmds_prologue() {
			local _var
			if [ "X$_transport" != "X" ]; then
				echo "'-c transport select $_transport'"
			fi
			# Generate list of array of prologues (if existing)
			for _var in "${OCDCTRL_TARGET_PROLOGUE[@]}"; do echo "'-c $_var'"; done
		}
		# Generate a list of epiloggue commands
		function cmds_epilogue() {
			local _var

			# Generate list of array of epilogues (if existing)
			for _var in "${OCDCTRL_CFG_EPILOGUE[@]}"; do echo "'-c $_var'"; done
		}

		# Print command exactly as-is to be run by eval as that will execute
		# exacly as printed (gets around shell-expansion)
		function print_cmd() {
			echo screen -dmS $_session_name openocd \
				-d${OCDCTRL_BACK_VERBOSITY-"3"} \
				-f "${OCDCTRL_CFG_INTERFACE}" \
				$(cmds_prologue) \
				"'-c gdb_port ${OCDCTRL_PORT_GDB-3333}'" \
				"'-c telnet_port ${OCDCTRL_PORT_TELNET-4444}'" \
				"'-c tcl_port ${OCDCTRL_PORT_TCL-6666}'" \
				-f "${OCDCTRL_CFG_TARGET}" \
				$(cmds_epilogue)
		}

		if [ "X$(screen -ls | grep $_session_name)" == "X" ]; then
			LOGI "Starting gdbserver"
			LOGV $(print_cmd)
			eval $(print_cmd)


			sleep 1
			_status=$(service_status)

			if [ $? -eq 0 ]; then
				LOGI "Server is $_status"
			else
				LOGE "Starting server [${OCDCTRL_PROJECT}] failed ($_status). Check connection/settings"
				return 1
			fi

		else
			LOGI "gdbserver [${OCDCTRL_PROJECT}] already running"
			return 0
		fi
	}

	function stop() {
		local _status
		_status=$(service_status)

		if [ $? -eq 0 ]; then
			LOGI "Stopping gdbserver"
			screen -ls | \
				grep $_session_name | \
				cut -d. -f1 | \
				awk '{print $1}' | \
				xargs kill

			sleep 1
			_status=$(service_status)

			if [ $? -ne 1 ]; then
				LOGE "Stopping server failed"
				return 1
			else
				LOGI "Server is $_status"
			fi

		else
			LOGI "gdbserver already stopped"
		fi
		return 0
	}

	function attach() {
		if [ X$_direct_attach == Xyes ]; then
			if [ "X$(grep screen <<< ${TERM})" == "X" ]; then
				LOGI "Attaching to screen session back-end OpenOCD service"
				screen -rd $_session_name
			else
				LOGE "Refusing attaching directly from terminal multiplexor"
			fi
		else
			local _port=${OCDCTRL_PORT_TELNET-4444}
			telnet localhost $_port
			return 0
		fi
	}

	function gdbserver_process_one() {
		local _cmd=$(tr '[:upper:]' '[:lower:]' <<< "${1}")

		if [[ ! " ${_subcommands[@]} " =~ " ${_cmd} " ]]; then
			LOGC "Unknown sub-command [$_cmd]"
			return 1
		fi

		$_cmd "${@:2}"
	}

	if [ X$_show_status == Xyes ]; then
		local _status
		_status=$(service_status)
		echo "  * $_status"
		return 0
	fi
	if [ X$_list_all == Xyes ]; then
		screen -ls | grep gdbserver_ | awk '{print "  " $1}'
		return 0
	fi

	__ocdctrl_handle_input -a "start" -f gdbserver_process_one -- $@

	return $?
}

if ! (return 0 2>/dev/null); then
	__ocdctrl_gdbserver "${@}"
	exit $?
fi
