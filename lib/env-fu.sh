#! /bin/bash
#
# print environment
#
# 1.
#   Environment from environment-file ${HOME}/.config/${ENV_PROJ}/settings if file
#   exists.
#   If file doesn't exist, it's created with defaults and with warning to
#   stderr.
# 2.
#   Pick up any local .env-file if existing
#
# env.sh is project agnostic. I.e. it can be re-used as a config library
# by any project.

if ! (return 0 2>/dev/null); then
	# Adapt these ENV-variables for each source-project env.sh is used for
	# in the source-file which is either sourcing or executing env.sh
	#----------------------------------------------------------------
	ENV_BASE='.' ## Version-specific Template config-files are here.
	ENV_PROJ=${ENV_PROJ-'dummy-project'} ## Name of your project
	ENV_LOCAL=${ENV_LOCAL-'.local.env'} ## File-name in current dir
	ENV_EXPORT=${ENV_EXPORT-'no'} ## Prepend 'export' to all settings
	#----------------------------------------------------------------

	source $(dirname $(readlink -f $0))/log.sh
fi

function _env() {
	local _config_dir="${HOME}/.config/${ENV_PROJ}"

	function get_config_dir() {
		echo $_config_dir
	}

	function set_config_dir() {
		LOGD "Set-dir: [$1]"
		_config_dir="$1"
	}

	# function update_config
	#
	# Operation:
	# * For each mentioned variable in file, the corresponding variable's
	#   value from environment (i.e. memory) will be put on right-hand-side
	# * Existing file-variables determines what is saved and what's not. I.e.
	#   if some-such wasn't mentioned, it will not be saved.
	# * Existing file-variables determines the value "type" (quoting). I.e.
	#   if file-variable RHS was surrounded with quotes of some sort, update
	#   will have quotes (strings), if not, update will also lack them.
	# * Variables not existing in memory, but mentioned and active in file
	#
	# Remarked variables:
	# * If a variable in memory was NOT found, line will remain as it was.
	#   i.e. if previously remarked it will STAY remarked
	# * If variable found:
	#    * RHS of "=" will be replaced with value from memory regardless of
	#      what was previously there
	#    * Any remarks will be removed, i.e. variable will start take effect
	#      upon next source.
	#
	# NOTES:
	# * To revert back an un-remarked (activated) line, this function cant do
	#   it.
	# * Variables not existing in memory, but mentioned and active in file
	#   (i.e. not remarked) will remain "as-is", effectively meaning that any
	#   non-remarged variables in the template file settings.env will become
	#   a default
	function update_config() {

		# Save relevant current environment in file (arg #1)
		# File must exist prior calling (i.e. this is rather update than save)
		function _file() {
			local _name=$(basename $1)
			local _file="${_config_dir}/$1"
			local _subdir=$(dirname $_file)
			local _var _val
			local _regx_val_lines='^[#]?[[:space:]]*[^[:space:]]+='

			# Produce a list of all RHS:s, stripped of remarks
			function file_vars() {
				cat $_file | \
					grep -Ee "$_regx_val_lines" | \
					sed -Ee 's/^#//' -e's/=.*//'
			}

			# Update RHS of variable in file and activate (un-remark) it if needed
			# Arg1 = key
			# Arg2 = value
			function update_var() {
				local _key=$1
				local _value="$2"
				local _quote_char
				local _regx_val_line='^[#]?[[:space:]]*'$_key'='
				local _regex='[[:digit:]]+'
				local _rhs

				local _cur_rhs=$(cat $_file | \
					grep -Ee "$_regx_val_line" | \
					sed -Ee 's/.*=//')

				_quote_char=${_cur_rhs:0:1}
				if [[ $_quote_char =~ [[:alnum:]] ]]; then
					unset _quote_char
				fi

				LOGD "[$_key=$_cur_rhs] => [$_key=${_quote_char}${_value}${_quote_char}]"

				_value=$(sed -Ee 's/\//\\\//g' <<< $_value) ## Fix '/' for sed...
				_rhs="${_quote_char}${_value}${_quote_char}"

				# Update RHS and un-remark in one go
				sed -Ei "/$_regx_val_line/s/"'^(#[[:space:]]*)*(.*=)(.*)'"/\2${_rhs}/" \
					$_file

			}

			for _var in $(file_vars); do
				# Note the '$'
				if [ -v $_var ]; then
					_val=$(eval echo \$$_var)
					LOGI "Yes [$_var]=$_val"

					# Quote arguments just in case to prevent clobber-expansion
					update_var "$_var" "$_val"
				else
					LOGI "No [$_var]"
				fi
			done
		}

		# Convenience wrapper to handle cases where settings.ini exist in
		# sub-directories
		function settings() {
			local _file_name=settings.env
			[ $# -gt 0 ] && _file_name=$1/$_file_name

			_file $_file_name
		}

		function constants() {
			local _file_name=constants.env
			[ $# -gt 0 ] && _file_name=$1/$_file_name

			_file $_file_name
		}

		${@}
	}

	# Check if exists. If not, copy template from BASE
	function check_create() {

		# Get version in directory (line from file "version.env"), directory as argument
		# If file is missing, return path instead so that comparisons of two
		# different directories where file is missing in either will always return false.
		function version_indir() {
			local _vdir="${1}"

			if [ -f "${_vdir}/version.env" ]; then
				cat "${_vdir}/version.env" | \
					grep -Ee'^VERSION=' | \
					cut -f2 -d"="
			else
				echo "${_vdir}/version.env"
			fi
		}

		function envdir() {
			if [ ! -d "${_config_dir}" ]; then
				LOGI "Creating missing dot-directory: [${_config_dir}]"
				mkdir -p "${_config_dir}"
				check_create envfile version.env
			elif [ $(version_indir "${_config_dir}") != \
				$(version_indir "${ENV_BASE}/env/") ];
			then
				local _ts=$(date +%s.%N)
				local _curr_version=$(version_indir "${_config_dir}" | \
					sed -e 's/"//g')
				local _bkup_dir="${_config_dir}_${_curr_version}_${_ts}"

				LOGW "PROJECT VERSION CHANGE DETECTED\n " \
					" Resetting environment files in [${_config_dir}]\n" \
					"  with project defaults. Backups in: [$_bkup_dir]"

				mv "${_config_dir}" "${_bkup_dir}"
				mkdir -p  "${_config_dir}"
				check_create envfile version.env

			fi
		}

		function envfile() {
			local _name=$(basename $1)
			local _file="${_config_dir}/$1"
			local _subdir=$(dirname $_file)

			if [ ! -f "${_file}" ]; then
				LOGI "Creating missing $_name-file: " "[${_file}]"

				if ! [ -d $_subdir ]; then
					LOGI "Creating missing sub-directory [${_subdir}]"
					mkdir -p $_subdir
				fi

				cp "${ENV_BASE}/env/$_name" "${_file}"
				update_config _file $1
			fi
		}
		if ! [ -d "$ENV_BASE/env" ]; then
			LOGC "Environment template directory missing. Please check your installation"
			exit 2
		fi
		${@}
	}

	function show() {

		# Prints file as one which can be evaluated, optionally exportable
		# (ENV_EXPORT)
		function _print_envfile() {
			local _file="${1}"
			cat "${_file}"
		}

		# Configuation settings
		# If arg is given:
		#  Arg #1: sub-project
		function settings() {
			local _file_name=settings.env
			[ $# -gt 0 ] && _file_name=$1/$_file_name

			check_create envdir
			check_create envfile $_file_name
			_print_envfile "${_config_dir}/$_file_name"
		}

		# Light-weight local environment: overloads from default
		function env_local() {
			local _this_dir="$(pwd)"

			# Pick up local environment if existing
			if [ -f "${_this_dir}/${ENV_LOCAL}" ]; then
				_print_envfile "${_this_dir}/${ENV_LOCAL}"
			fi
		}

		# "constants" environment
		function constants() {
			check_create envdir

			check_create envfile constants.env
			_print_envfile "${_config_dir}/constants.env"

			check_create envfile version.env

			# Transform VERSION to <ENV_PROJ>_VERSION
			_print_envfile "${_config_dir}/version.env" | \
				sed -Ee "s/^VERSION=/$(tr '[:lower:]' '[:upper:]' <<<${ENV_PROJ})_VERSION=/"

			check_create envfile postfix_env.sh
			source "${_config_dir}/postfix_env.sh"

			echo "ENV_BASE=${ENV_BASE}"
			echo "ENV_PROJ=${ENV_PROJ}"
		}

		all() {
			#Order determines presidency (when evaluated)
			_env show settings $@
			_env show env_local
			_env show constants
		}
		${@}
	}
	if ! [ -v ENV_BASE ]; then
		LOGC "ENV_BASE must be set. Please check your installation"
		exit 2
	fi
	${@}
}

if ! (return 0 2>/dev/null); then
	case $1 in
		show)
			_env show ${@:2}
			;;
		*)
			_env show all $@
			;;
	esac
fi
