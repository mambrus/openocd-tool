#!/bin/bash

if ! (return 0 2>/dev/null); then
	source ocdctrl.sh ## Try source (if possible) to get the trampoline
	__ocdctrl_source_trampoline log
	__ocdctrl_source_trampoline misc
	__ocdctrl_source_trampoline getopts-fu
	__ocdctrl_source_ocdctrl
fi
ocdctrl_cmd[flash]='user'

function synopsis_flash() {
	cat <<-EOF
	flash [OPTION]... [COMMAND]...
	EOF
}

function usage_flash() {
	cat <<-EOF
	flash [-l | [OPTION]... [COMMAND]...]
	EOF
}

function help_flash() {
	cat <<-EOF
	NAME
	   ocdctrl-flash

	SYNOPSIS
	   $(synopsis_flash)

	DESCRIPTION
	   Flash a file to target

	EXAMPLES
	   flash -h
	      Shows help for flash

	OPERATION
	   This is ocdctrl-flash

	OPTIONS
	   -h                 This help
	   -e                 Formatted image, usually ELF

	ARGUMENTS
	   Images

	SEE ALSO
	   Related commands:
	$(__ocdctrl_help -j6 -tu $(awk '{print "-i " $0}' <<< ${ocdctrl_cmd[flash]}))

	   Back-end command:
	      $(__ocdctrl_help -j6 -u list)

	AUTHOR
	   Michael Ambrus <michael@helsinova.se>, Sept 2019
	   Michael Ambrus <ambrmi09@gmail.com>, Januari 2001 (BUS)

	EOF
}

function getopts_flash() {
	while getopts $_options OPTION; do
		case $OPTION in
		e)
			_formatted=yes
			;;
		esac
	done
	OPTIND=1

	return $?
}

function __ocdctrl_flash() {
	local _formatted

	_getopts_begin e help_flash
		getopts_flash "${@}"
	_getopts_end "${@}"
	shift $(($OPTIND - 1))

	# Preferred flash method (bin). Because its faster and doesnt erase more than it
	# needs to)
	function flash_bin() {
		LOGI "Flashing binary"
		__ocdctrl_cmd reset halt                  && \
		__ocdctrl_cmd flash probe 0               && \
		__ocdctrl_cmd stm32f1x mass_erase 0       && \
		__ocdctrl_cmd flash write_bank 0 $1 0     && \
		__ocdctrl_cmd reset halt                  && \
		LOGI "Flashed [$1] OK"
	}

	# Generic formatted flash. Lets OpenOCD figure out the format
	function flash_form() {
		LOGI "Flashing formatted"
		__ocdctrl_cmd reset halt                  && \
		__ocdctrl_cmd flash probe 0               && \
		__ocdctrl_cmd stm32f1x mass_erase 0       && \
		__ocdctrl_cmd flash write_image 1 1 $1    && \
		__ocdctrl_cmd reset halt                  && \
		LOGI "Flashed [$1] OK"
	}

	# Absolute path of argument
	function abspath() {
		local _last="${1: -1}"
		local _dir
		local _absdir
		local _base

		if [ $_last == "/" ]; then
			_dir=$1
		else
			_dir=$(dirname $1)
			_base=$(basename $1)
		fi

		if [ -d $_dir ]; then
			_absdir=$(cd $_dir; pwd)
			if [ -f "$_absdir/$_base" ]; then
				echo "$_absdir/$_base"
				return 0
			fi
			LOGE "File not found: $_absdir/$_base"
			return 1
		fi
		LOGE "Directory does not exist: $_absdir"
		return 1
	}

	function flash_process_one() {
		local _img_name=$(abspath $1)
		LOGI "Flashing: $_img_name"

		if [ X$_formatted == Xyes ]; then
			if [ "X${OCDCTRL_FLASH_FORM}" == X ]; then
				flash_form $_img_name
			else
				eval true
				${OCDCTRL_FLASH_FORM} $_img_name
			fi
		else
			if [ "X${OCDCTRL_FLASH_BIN}" == X ]; then
				flash_bin $_img_name
			else
				eval true
				${OCDCTRL_FLASH_BIN} $_img_name
			fi
		fi
	}

	__ocdctrl_handle_input -f flash_process_one -- $@

	return $?
}

if ! (return 0 2>/dev/null); then
	__ocdctrl_flash "${@}"
	exit $?
fi
