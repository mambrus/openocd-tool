#!/bin/bash

ENV_PROJ=${ENV_PROJ-'ocdctrl'} ## Name of your project
ENV_LOCAL=${ENV_LOCAL-'.local.env'} ## File-name in current dir
ENV_EXPORT=${ENV_EXPORT-'no'} ## Prepend 'export' to all settings

if ! (return 0 2>/dev/null); then
	source ocdctrl.sh ## Try source (if possible) to get the trampoline
	__ocdctrl_source_trampoline log
	__ocdctrl_source_trampoline env-fu
	__ocdctrl_source_trampoline misc
	__ocdctrl_source_trampoline getopts-fu
	__ocdctrl_source_ocdctrl
else
	__ocdctrl_source_trampoline env-fu
fi
ocdctrl_cmd[env]='core framework user'

function synopsis_env() {
	cat <<-EOF
	env [OPTION]... [COMMAND]...
	EOF
}

function usage_env() {
	cat <<-EOF
	env [-l | [OPTION]... [COMMAND]...]
	EOF
}

function help_env() {
	cat <<-EOF
	NAME
	   ocdctrl-env

	SYNOPSIS
	   $(synopsis_env)

	DESCRIPTION

	   Loading and alternating environment prior running command or
	   managing settings profiles

	EXAMPLES
	   One or a few environment variables preferably are set in some
	   shell-init. The following line omits the need for -p option to ocdctrl
	   (ocdctrl -p PROFILE)

	   export OCDCTRL_PROJECT=some-site

	OPERATION
	   One or several commands can be run under a new environment after
	   altering specific environment variable or saved as default in
	   settings-profie (-P,-s) or as local settings in current directory
	   (.env.local).

	OPTIONS
	    -h                        This help
	    -g                        Get (i.e print-out) current relevant
	                              environment
	    -P NAME                   Re-load settings from project init-profile
	                              Over-loads settings allready read so far
	                              (i.e.  ocdctrl -p)
	    -e                        Edit current or currently selected project
	                              settings and exit
	    -s                        Save settings to project init-profile
	                              Unless -p is used, profile-name is implicit
	                              (see -p or OCDCTRL_PROJECT)
	    -p NAME                   Set name of profile/project without loading
	                              it. Use in combination with -s to use one
	                              profile as template for another
	    -l                        List available profiles

	   Note: -p and -P are in conflict and you can not use both. If you need
	         one profile to start with for re-creating another, use
	         root-command's option for initial profile.

	ARGUMENTS
	   List of sub-commands (optional)

	AUTHOR
	   Michael Ambrus <michael@helsinova.se>, July 2019

	EOF
}

function __ocdctrl_edit_config() {
	if [ $# -ne 1 ]; then
		LOGE "Exactly one argument expected: project-name"
		exit 1
	fi
	if [ "X$EDITOR" == "X" ]; then
		LOGE "\$$EDITOR not set. Can't continue."
		exit 1
	fi
	if ! which $EDITOR 2>&1 >/dev/null; then
		LOGE "\$EDITOR [$EDITOR] not in \$PATH. Can't continue."
		exit 1
	fi
	$EDITOR \
		$(_env get_config_dir)/$1/settings.env \
		$(_env get_config_dir)/version.env \
		$(_env get_config_dir)/constants.env
}

function getopts_env() {
	local _opt_p_set
	while getopts $_options OPTION; do
		case $OPTION in
		g)
			_op_mode='show'
			;;
		P)
			_profile_name=$OPTARG
			_op_mode='get'
			if [ "X$_opt_p_set" != "X" ]; then
				echo "Options conflict detected. Use ocdctrl-env -h for help" >&2
				exit 1
			fi
			_opt_p_set='yes'
			;;
		e)
			__ocdctrl_edit_config ${_profile_name-${OCDCTRL_PROJECT}}
			exit 0
			;;
		s)
			_op_mode='set'
			;;
		i)
			_op_mode='set-local'
			;;
		l)
			_op_mode='list-profiles'
			;;
		p)
			OCDCTRL_PROJECT="$OPTARG"
			if [ "X$_opt_p_set" != "X" ]; then
				echo "Options conflict detected. Use ocdctrl-env -h for help" >&2
				exit 1
			fi
			_opt_p_set='yes'
			;;
		esac
	done
	OPTIND=1
}

function __ocdctrl_env() {
	local _rc=0
	local _cmd=0
	local _profile_name
	local _op_mode='show' ## Valid values: show, get, set, set-local

	_getopts_begin glP:esei:p: help_env
		getopts_env "${@}"
	_getopts_end "${@}"
	shift $(($OPTIND - 1))

	function opget() {
		load_environment ${_profile_name}
	}

	function opshow() {
		set | grep -Ee '^ENV_.*'
		set | grep -Ee '^LOG_LEVEL_.*'
		set | grep -Ee '^OCDCTRL_.*'
	}

	function opset() {
		case $_op_mode in
			set-local)
				LOGC "TBD"
				exit 1
				;;
			*)
				LOGI "Saving/updating configuration settings for [$OCDCTRL_PROJECT]"
				_env show constants >/dev/null ## Will create from template if not exist
				_env show settings $OCDCTRL_PROJECT >/dev/null ## Will create from template if not exist
				_env update_config constants
				_env update_config settings $OCDCTRL_PROJECT
				;;
		esac
	}

	function oplist() {
		function profiles() {
			local _dir=$(_env get_config_dir)

			if [ -d $_dir ]; then
				(
					cd $_dir
					find . -name settings.env | \
						xargs dirname | \
						xargs -I% basename % | \
						sort -u
				)
			fi
		}

		case $_op_mode in
			list-profiles)
				profiles
				;;
			*)
				LOGC "Bad op-mode [$_op_mode]"
				exit 1
				;;
		esac
	}

	case $_op_mode in
		get)
			opget
			;;
		set*)
			opset
			;;
		show)
			 [ $# -eq 0 ] && opshow
			;;
		list-*)
			 oplist
			;;
		*)
			LOGC "Bad op-mode [$_op_mode]"
			exit 1
			;;
	esac

	if [ $# -ne 0 ]; then
		for _cmd in $?; do
			if [ "${_cmd:0:1}" == '-' ]; then
				shift
				break
			fi
			load_environment ${_profile_name-${OCDCTRL_PROJECT}}
			__ocdctrl_do_cmd $@
		done
	fi

	return $_rc
}

if ! (return 0 2>/dev/null); then
	# pick up .env.local if possible, to at least get OCDCTRL_PROJECT name
	eval $(_env show env_local)
	__ocdctrl_env "${@}"
	exit $?
fi
