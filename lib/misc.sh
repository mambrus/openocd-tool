#!/bin/bash

# Looks for options for help (-h, --help) counting from the end to the first
# non-option. This enables requesting help for sub-commands without needing
# a profile for the help option
function __help_requested() {
	local -i _i

	for (( _i=$#; _i > 0; _i--)); do
		case "${!_i}" in
			-*)
				# List of other options then help are ingnorded
				if [ "X${!_i}" == "X-h" ] || [ "X${!_i}" == "X--help" ]; then
					return 0
				fi
				;;
			*)
				return 1
				;;
		esac
	done
}
function __help_not_requested() {
	__help_requested "${@}" || return 1
	return 0
}

# pass $FUNCNAME as $1 and function will return number of recursions
function nnested() {
	sed 's/[[:space:]]/\n/g' <<< ${FUNCNAME[@]} | grep $1 | wc -l
}

# Do not use wtth test ([]) unless combined with a value as test evauates
# an expression and as even a unary is a valid expression
#
# Example:
#	if __ocdctrl_stdin_isapipe; then
#		while read -r; do
#			for _sha in $REPLY; do
#				process_one $_sha
#			done
#		done
#	else
#
function __ocdctrl_stdin_isapipe() {
	[ -t 0 ] || return 0 && return 1
}

function __ocdctrl_do_cmd() {
	local cmd=${1-"gdbserver"}

	if [ "X$(type -t __ocdctrl_${cmd})" == "Xfunction" ]; then
		LOGV "Executing command: ${cmd} ${@:2}"
		__ocdctrl_$cmd "${@:2}"
	elif [ "X$(type -t __${cmd})" == "Xfunction" ]; then
		LOGV "Executing built-in function: __${cmd} ${@:2}"
		__$cmd "${@:2}"
	else
		echo "Unknown command: ${cmd}" >&2
		echo "Usage: ocdctrl [OPTIONS] [COMMAND [OPTIONS]..]" >&2
		exit 2
	fi

	if [ $? -eq 0 ]; then
		LOGV "Command/function OK: ${cmd} ${@:2}"
	else
		LOGE "Command/function fail [$?]: ${cmd} ${@:2}"
	fi

	return $? ## LOG now preserves $?. I.e. this is OK
}

# Loads an operating environment based on config-files, influential
# environment variables and the rules between them.
# Arg #1: NAME		: Profile name used to get config-settings.
#						: Example "git-scm"
function load_environment() {
	local _profile_name=$1

	# Sets derived globals. I.e. globals who's only dependency is a root
	# environment valuably
	function set_derived_globals() {
		function set_ippon_image() {

			return 0 ## Cannot fail. Warnings is existing in log
		}
		set_ippon_image

		return $?
	}

	function main_load() {
		LOGD "Using project configuration for [$_profile_name]"
		# Reload new environment from profile
		source <(_env show all $_profile_name)
		if [ $OCDCTRL_PROJECT != $_profile_name ]; then
			LOGW "Project-name conflict detected [$OCDCTRL_PROJECT != $_profile_name].\n" \
				"(Check possible conflict in .local.env). Choosing [$_profile_name]..."

			OCDCTRL_PROJECT=${_profile_name}
			_env update_config settings ${_profile_name}
			source <(_env show all $OCDCTRL_PROJECT) ## Settings corrected, but .local.env
															## may still apply
		fi
	}

	function handle_errors() {
		LOGC "Failed! Please check your logs for additional information."
		exit 1
	}

	main_load &&
		set_derived_globals ||
			handle_errors

	OCDCTRL_PROFILE_LAST=$_profile_name
}

# Small helper commands
# =====================

# Disk use of directory in PV compatimle format
# Arg 1: directory
function __du() {
	du -sh "${1}" | awk '{print $1}'
}

# Total disks usage in KB
function __du_full() {
	pushd / >/dev/null
	df  | awk '
		BEGIN {
			total_used = 0
			total_free = 0
		}
		{
			total_used = total_used + $3
			total_free = total_free + $2
		}
		END {
			print total_free
			print total_used
			print total_free - total_used
		}
	'
	popd >/dev/null
}

# Remark space column delimiter
#
# In table outputs where ASCII-space is used as a delimiter but column
# content also has spaces, identifying a column by number is not so
# trivial. This function replaces all occurrences of space **larger than
# two** with marker provided as option -m
# Alternatively the operation can be the opposite, i.e. column marker
# remain, but the marker is used to replace spaces in column text instead
# (-x).
#
# Be very choosy when you pick a marker and make sure it doesn't exist in
# Any of the column contents. This is especially true when combined with the
# -m option as occurances will create one more column for each conflict. The
# '#'-character is usually a good marker for that purpose.
#
# NOTE:
#  - If the new marker is a valid regexp-pattern, it must be escaped
function __remark_column_spaces() {
	local _marker='_'
	local _invert

	OPTERR=1
	OPTIND=1

	while getopts m:x OPTION; do
		case $OPTION in
		m)
			_marker=$OPTARG
			;;
		x)
			_invert='yes'
			;;
		?)
			echo "${FUNCNAME}: Option unknown: [$OPTION]" 1>&2
			exit 2
			;;
		esac
	done
	shift $(($OPTIND - 1))

	function remark() {
		sed -Ee 's/ {2}/'$_marker$_marker'/g' -e 's/'$_marker' /'$_marker$_marker'/g'
	}

	# Inverts separator and whites but maintains column width (i.e. same
	# total line width).
	#
	# This can't be done n 2 passes without introducing a 3:rd character.
	# I.e.  both character alternatives must be processed simultaneously
	function invert() {
		awk -v marker=$_marker '
			{
				line=$0
				linesz=length(line);
				for (i=1; i<=linesz; i++) {
					c=substr(line,i,1)

					if (c == marker )
						printf(" ");
					else if (c == " " )
						printf("%c", marker)
					else
						printf("%c", c)
				}
				printf("\n");
			}
		'
	}

	if [ -v _invert ]; then
		remark | invert
	else
		remark
	fi
}

# Not-so small helper commands
# ============================

# Handle input from either stdin or from arg-list
#
# Note: that this function does NOT use getopts-fu (avoid complexity)
# -f FUNCTION			Funtion to call
# -a ARG					Default argument if no input
# -s 					 	One call instead of one per input. If read from
#							- argv:  Pass arguments to FUNCTION "as-is"
#							- stdin: Squash into one arg-array, then pass it as one
#							  call.
# -p VALUE				Don't test for pipe on stdin but use VALUE. I.e. -p1
#                    means there is a pipe and you should read from stdin
#							and -p0 meand there isn't one and you should never
#							try reading from stdin
# --                 Break arg-parsing and pass the rest of the
#                    command-line to FUNCTION
function __ocdctrl_handle_input() {
	local _function
	local _default_arg
	local _squash
	local -i _fake_pipeval

	OPTERR=1
	OPTIND=1

	while getopts f:a:sp: OPTION; do
		case $OPTION in
		f)
			_function=$OPTARG
			;;
		a)
			_default_arg=$OPTARG
			;;
		s)
			_squash='yes'
			;;
		p)
			_fake_pipeval=$OPTARG
			if [ $_fake_pipeval -ne 0 ] && [ $_fake_pipeval -ne 1 ]; then
				echo "${FUNCNAME}: Invalid option argument to -p [$OPTION]" 1>&2
				exit 2
			fi
			;;
		?)
			echo "${FUNCNAME}: Option unknown: [$OPTION]" 1>&2
			exit 2
			;;
		esac
	done
	shift $(($OPTIND - 1))

	if ! [ -v _function ]; then
		echo "${FUNCNAME}: Option -f is mandatory" 1>&2
		exit 2
	fi
	if [ "X$(type -t ${_function})" != "Xfunction" ]; then
		echo "${FUNCNAME}: Unknown function as option argument -f: [${_function}]" >&2
		exit 2
	fi

	function from_pipe() {
		if [ -v _fake_pipeval ]; then
			case $_fake_pipeval in
				1)
					return 0
					;;
				0)
					return 1
					;;
				*)
					echo "${FUNCNAME}: Invalid option argument to -p [$OPTION]" 1>&2
					exit 2
					;;
			esac
		else
			__ocdctrl_stdin_isapipe
			return $?
		fi
	}

	local _input
	local -a _argv
	local -i _argi=0

	# Determine what/from-where to process
	if [ $# -eq 0 ]; then
		if from_pipe; then
			if [ -v _squash ]; then
				while read -r; do
					for _input in $REPLY; do
						_argv[$_argi]=$_input
						_argi=$(( _argi + 1 ))
					done
				done
				$_function ${_argv[@]}
			else
				while read -r; do
					for _input in $REPLY; do
						$_function $_input
					done
				done
			fi
		else
			$_function $_default_arg ## Normal use-case
		fi
	else
		if [ -v _squash ]; then
			$_function "$@"
		else
			for _input in $@; do
				$_function $_input
			done
		fi
	fi

	return $?
}
