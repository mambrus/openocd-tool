#! /bin/bash
#
# Cascadeable options parsers for functions

function _getopts_begin() {
	_options="vh${1}"
	_help=${2}
	OPTERR=0
	OPTIND=1
	return 0
}

#==========================================================
# Set this in owning project but auto-set with something to
# satisfy help2man even if not set
[ -v GETOPTSFU_VERSION ] || GETOPTSFU_VERSION='<none>'
#==========================================================

function _getopts_end() {
	OPTERR=1
	OPTIND=1
	local _rc=0
	local _ignore=$(
		cut -c2- <<<$_options | \
		sed -E \
			-e 's/://g' \
			-e 's/(.)/\1|/g' \
			-e 's/.$//')

	source <(cat -- <<-EOF
		while getopts $_options OPTION; do
			case \$OPTION in
			v)
				echo "$GETOPTSFU_VERSION"
				exit 0
				;;
			h)
				if [ -t 1 ]; then
					$_help | less -R
				else
					$_help
				fi
				exit 0
				;;
			$_ignore)
				;;
			?)
				echo "Option unknown" 1>&2
				echo "For help, type: ocdctrl [-h] | [cmd -h]" 1>&2
				exit 2
				;;
			esac
		done
	EOF
	)

	unset _help
	unset _options
	return $_rc
}

function getopts_negate() {
	local _rc=0
	OPTIND=1

	while getopts n OPTION; do
		case $OPTION in
		n)
			echo "local _negate='yes'"
			;;
		esac
	done
	return $_rc
}

function getopts_force() {
	local _rc=0
	OPTIND=1

	while getopts $_options OPTION; do
		case $OPTION in
		f)
			echo "local _force='yes'"
			;;
		esac
	done
	return $_rc
}

# Test & example of usage
function __fun() {
	_getopts_begin nfi:o: ocdctrl_help
		eval $(getopts_force "${@}")
		eval $(getopts_container-create "${@}")
	_getopts_end "${@}"
	shift $(($OPTIND - 1))

	LOGD "F=$_force, N=$_negate / $OCDCTRL_INFILE / $OCDCTRL_OUUTFILE"
	echo "$@"
	exit 0
}

