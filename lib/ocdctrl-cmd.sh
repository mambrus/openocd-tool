#!/bin/bash

if ! (return 0 2>/dev/null); then
	source ocdctrl.sh ## Try source (if possible) to get the trampoline
	__ocdctrl_source_trampoline log
	__ocdctrl_source_trampoline misc
	__ocdctrl_source_trampoline getopts-fu
	__ocdctrl_source_ocdctrl
fi
ocdctrl_cmd[cmd]='user'

function synopsis_cmd() {
	cat <<-EOF
	cmd [OPTION]... [COMMAND]...
	EOF
}

function usage_cmd() {
	cat <<-EOF
	cmd [-l | [OPTION]... [COMMAND]...]
	EOF
}

function help_cmd() {
	cat <<-EOF
	NAME
	   ocdctrl-cmd

	SYNOPSIS
	   $(synopsis_cmd)

	DESCRIPTION
	   RPC command on OpenOCD server

	EXAMPLES
	   cmd -h
	      Shows help for cmd

	OPERATION
	   This is ocdctrl-cmd

	OPTIONS
	   -h                 This help
	   -i                 Verbose i.e. response interaction to stdout
	   -t seconds         Timeout with error RPC exceeds seconds
	   -l                 List back-end commands. With argument, list
	                      specific cammand and it's sub-commands
	   -u                 Same as -i but parserable output

	ARGUMENTS
	   OpenOCD command string

	SEE ALSO
	   Related commands:
	$(__ocdctrl_help -j6 -tu $(awk '{print "-i " $0}' <<< ${ocdctrl_cmd[cmd]}))

	   Back-end command:
	      $(__ocdctrl_help -j6 -u list)

	AUTHOR
	   Michael Ambrus <michael@helsinova.se>, Februari 2020
	   Michael Ambrus <ambrmi09@gmail.com>, Januari 2001 (BUS)

	EOF
}

function getopts_cmd() {
	while getopts $_options OPTION; do
		case $OPTION in
		i)
			_verbose=1
			;;
		l)
			_list_cmd='yes'
			;;
		t)
			_timeout=$OPTARG
			;;
		u)
			_parserable='yes'
			_list_cmd='yes'
			;;
		esac
	done
	OPTIND=1

	return $?
}

#
# Execute anything on the Jim engine
#
# Arguments:
# 1: 	(0|1) 	Print the response
# 2-n: 	STRING	Anything to be executed "as is"
#
function __ocdctrl_rpc() {
	local _show_response=$1
	shift
	local _cmd="$@"
	local _port=${_port-${OCDCTRL_PORT_TCL-6666}}
	local _timeout=${_timeout-2}
	local _session_name=${_session_name-"ocdctrl_rpc"}

	function rpc_cmd() {
		expect -f <(cat -- <<-EOF
			exp_internal 0
			log_user 0
			#exp_internal -f "/tmp/$_session_name-int.log" 0
			set logfile [open "/tmp/$_session_name.log" "a"]

			proc msleep {time} {
				after \$time set end 1
				vwait end
			}

			set stty_init raw
			spawn nc localhost $_port

			set timeout $_timeout
			puts \$logfile "Sending: $_cmd \n"

			if {$_show_response} {
				send -i \$spawn_id "capture \"$_cmd\"\x1a"
			} else {
				send -i \$spawn_id "$_cmd\x1a"
			}

			log_user $_show_response
			expect {
				timeout {
					puts \$logfile "Error: timeout after ${_timeout}s"
					flush \$logfile
					exit 1
				}
			   \x1a {
					puts \$logfile "\$expect_out(buffer)"
					puts \$logfile "Netcat responed as expexted"
				}
			}
			puts \$logfile "<-- Command executed"
			flush \$logfile

			exit 0

		EOF
		) | sed -e $'s/[^[:print:]\t]//g'	`# Remove any non-printables` \
				-e 's/^.*^Z//'				 # Filter the echoed ^Z (\x1a)
	}
	(
		rpc_cmd
		echo 		# Add an extra empy line, it will be removed later
	) | tac | 		# Revert output so any empty comes first
		sed -En '/[^[:space:]]/,$P' | # Start print from from first line containing
									  # any text and to the EOF
		tac 		# Revert order again
}

function __ocdctrl_cmd() {
	local _verbose
	local _list_cmd
	local _parserable
	local _timeout=2
	local _session_name="gdbserver_${OCDCTRL_PROJECT}"
	local _port="${OCDCTRL_PORT_TCL-6666}"

	_getopts_begin it:lu help_cmd
		getopts_cmd "${@}"
	_getopts_end "${@}"
	shift $(($OPTIND - 1))

	#
	# Refine output from OpenOCD 'usage' command into parseable lines
	#
	# * Start output from first line that begins with the cmd
	# * Concatenate following lines that belongs to same command
	# * Stop output for anything else but continue searching for start
	#
	function usage_extractor() {
		local _cmd="$@"

		awk -f <(cat -- <<-EOF
			function ltrim(s) {
				sub(/^[ \t\r\n]+/, "", s);
				return s;
			}
			function rtrim(s) {
				sub(/[ \t\r\n]+$/, "", s);
				return s;
			}
			function trim(s) {
				return rtrim(ltrim(s));
			}
			function lcut(s, len) {
				return substr(s, 1, len)
			}

			BEGIN {
				in_cmd=0;
				buffer="";
			}

			/^[[:space:]]*$_cmd[[:space:]]*/{
				if (in_cmd) {
					printf("%s\n",rtrim(buffer));
					buffer="";
				}
				buffer = buffer \$0 " "
				in_cmd=1;
			}

			/^[[:space:]]*[(<[]/{
				if (in_cmd) {
					buffer = buffer ltrim(\$0) " "
				}
			}
			
			# If neithe or the above, flush buffer if needed and terminate
			# state
			!/^[[:space:]]*$_cmd[[:space:]]*/ && !/^[[:space:]]*[(<[]/{
				if (in_cmd) {
					printf("%s\n",rtrim(buffer));
					buffer="";
				}
				in_cmd=0;
			}

			END {
				if (in_cmd) {
					printf("%s\n",rtrim(buffer));
					buffer="";
				}
			}
		EOF
		)
	}
	#
	# Refine output from OpenOCD 'usage' in user friendly output
	# i.e. non parserable
	#
	# * Start output from first line that begins with the cmd
	# * Stop output for anything else but continue searching for start
	function usage_extractor_uif() {
		local _cmd="$@"

		awk -f <(cat -- <<-EOF
			BEGIN {
				in_cmd=0;
			}

			/^[[:space:]]*$_cmd[[:space:]]*/{
				print
				in_cmd=1;
			}

			/^[[:space:]]*[(<[]/{
				if (in_cmd) {
					print
				}
			}
			
			# If neithe or the above, terminate the cycle
			# state
			!/^[[:space:]]*$_cmd[[:space:]]/ && !/^[[:space:]]*[(<[]/{
				in_cmd=0;
			}
		EOF
		)
	}

	function cmd_process_one() {
		local _cmd="$@"

		LOGD "OpenOCD RPC: $_cmd"
		__ocdctrl_rpc ${_verbose-0} "$_cmd"
	}

	if [ X$_list_cmd == 'Xyes' ]; then
		_verbose=1
		_timeout=7

		# List top-level commands
		function main_cmds() {
			cmd_process_one help | grep -Eve '^[[:space:]]' | cut -f1 -d' '
		}

		# List command (arg #1) and it's sub-level commands
		function cmd_and_sub() {
			if [ X$_parserable == 'Xyes' ]; then
				cmd_process_one "usage $1" | \
					usage_extractor "$@"
			else
				cmd_process_one "usage $1" | \
					usage_extractor_uif "$@"
			fi
		}

		if [ $# -eq 0 ]; then
			main_cmds
		else
			cmd_and_sub "$@"
		fi
	else
		__ocdctrl_handle_input -s -a "dummy" -f cmd_process_one -- $@
	fi

	return $?
}

if ! (return 0 2>/dev/null); then
	__ocdctrl_cmd "${@}"
	exit $?
fi
