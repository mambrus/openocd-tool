#!/bin/bash

if ! (return 0 2>/dev/null); then
	source ocdctrl.sh ## Try source (if possible) to get the trampoline
	__ocdctrl_source_trampoline log
	__ocdctrl_source_trampoline misc
	__ocdctrl_source_trampoline getopts-fu
	__ocdctrl_source_ocdctrl
fi
ocdctrl_cmd[help]='core framework user'

function synopsis_help() {
	cat <<-EOF
	help [OPTION]... [COMMAND]...
	EOF
}

function usage_help() {
	cat <<-EOF
	help [-l | [OPTION]... [COMMAND]...]
	EOF
}

function help_help() {
	cat <<-EOF
	NAME
	    ocdctrl-help

	SYNOPSIS
	    $(synopsis_help)

	DESCRIPTION
	    Display help about command, list of command or command
	    type-introspection

	EXAMPLES
	    help -i
	        Shows list of commands

	    help -m <<< 'help'
	        Show this help

	    help  -ls -i3
	        Show condendsed list of commands and their synopsis where each
	        command is indented by 3 and each commands corresponding synopsis
	        is indented by another 3.

	OPERATION
	    Inspect help for sub-commands or output lists of commands. Input is
	    received from either arguments or stdin.

	    Lists are generated with either of the flags -l, -a, -t

	    If no input is given and no list is required, this help is shown

	OPTIONS
	    -h                 This help
	    -l                 List avaiable commands
	    -a                 List all avaiable commands including internal
	    -t                 Introspection: List registered category-types
	                       If combined with -c, this option will generate a
	                       list of commands with it's respective category-types
	    -i category        Filter introspection-list including only category
	                       This option can be given multiple times
	    -I                 List from multiple -i are OR:ed (more hits)
	    -x category        As -i but eXcluding. If both filters given (-i, -x),
	                       the first mentioning will determine which filter is
	                       applied before the other
	    -X                 Exlude only those where any element in exlude-list
	                       (multiple -x) are in command's categories (more hits)
	    -j N               Indent output N number of spaces
	    -s                 Show synopsis
	    -u                 Show usage
	    -c                 Show command-name
	    -m                 Show man-page

	ARGUMENTS
	   If given, all argumens are sub-commands to inspect for help

	AUTHOR
	   Michael Ambrus <michael@helsinova.se>, Augusti 2019

	EOF
}

function getopts_help() {
	local _rc=0

   # Values to be optionally set by option arguments
	local filter_exclude
	local capital_exclude
	local filter_include
	local capital_include
	local filter_first
	local include_internal
	local list_all
	local list_types
	local more_about
	local show_cmd
	local show_manpage
	local show_synopsis
	local show_usage
	local use_filter

	while getopts $_options OPTION; do
		case $OPTION in
		j)
			nspaces="$OPTARG "
			;;
		a)
			list_all='yes'
			include_internal='yes'
			;;
		l)
			list_all='yes'
			include_internal='no'
			;;
		t)
			list_all='yes'
			list_types='yes'
			;;
		i)
			use_filter='yes'
			filter_include="${filter_include} $OPTARG"   # Note that there will be
                                                      # a leading space
			[ -v filter_first ] || filter_first='include'
			;;
		I)
			capital_include='yes'
			;;
		x)
			use_filter='yes'
			filter_exclude="${filter_exclude} $OPTARG"   # Note that there will be
                                                      # a leading space
			[ -v filter_first ] || filter_first='exclude'
			;;
		X)
			capital_exclude='yes'
			;;
		c)
			show_cmd='yes'
			;;
		s)
			more_about='yes'
			show_synopsis='yes'
			;;
		u)
			more_about='yes'
			show_usage='yes'
			;;
		m)
			more_about='yes'
			show_manpage='yes'
			;;
		esac
	done
	OPTIND=1

   # Pass on what has been set in the loop above
	[ -v nspaces ] && echo local nspaces=$nspaces
	[ -v filter_exclude ] && echo local filter_exclude=\"$filter_exclude\"
	[ -v capital_exclude ] && echo local capital_exclude=\"$capital_exclude\"
	[ -v filter_include ] && echo local filter_include=\"$filter_include\"
	[ -v capital_include ] && echo local capital_include=\"$capital_include\"
	[ -v filter_first ] && echo filter_first=$filter_first
	[ -v include_internal ] && echo local include_internal=$include_internal
	[ -v list_all ] && echo local list_all=$list_all
	[ -v list_types ] && echo local list_types=$list_types
	[ -v more_about ] && echo local more_about=$more_about
	[ -v show_cmd ] && echo local show_cmd=$show_cmd
	[ -v show_manpage ] && echo local show_manpage=$show_manpage
	[ -v show_synopsis ] && echo local show_synopsis=$show_synopsis
	[ -v show_usage ] && echo local show_usage=$show_usage
	[ -v use_filter ] && echo local use_filter=$use_filter

	return $_rc
}

function __ocdctrl_help() {
	_getopts_begin j:altsucmIi:Xx: help_help
		eval $(getopts_help "${@}")
	_getopts_end "${@}"
	shift $(($OPTIND - 1))

	function indent() {
		awk -v nspaces=$nspaces '{
			for (i=0; i<nspaces; i++) {
				printf(" ");
			}
			print $0;
		}'
	}

	function list_commands() {

		function all() {
			set | \
				grep -Ee '^__.*()' | \
				sort | \
				sed -Ee 's/^__//' -e 's/\(\)[[:space:]]*$//'
		}

		function qualified() {
			# Keys in this array can't include white-spaces which we can
			# safely make use of: I.e. to output a <cr> delimitered list

			echo ${!ocdctrl_cmd[@]} | tr ' ' '\n' | sort
		}

		function types() {
			local _key _val _cmd_width=0

			# Sort values of key **in-line**
			# (i.e. Output is ONE sorted line)
			function sorted_values() {
				echo $@ | \
					tr ' ' '\n' | \
					sort | \
					awk '{
						printf("%s ",$0)
					}' | \
					sed -Ee 's/.$//'
			}

			function command_list() {
				local _key _cmd_width=0

				# Find out width for nicer formatting
				for _key in $(qualified); do
					if [ ${#_key} -gt $_cmd_width ]; then
						_cmd_width=${#_key}
					fi
				done

				# Print the list
				(for _key in $(qualified); do
					printf "%-${_cmd_width}s : %s\n" $_key \
						"$(sorted_values ${ocdctrl_cmd[$_key]})"
				done) | sort -u
			}

			function core_list() {
				local _val
				# Note the "'s
				(for _val in "${ocdctrl_cmd[@]}"; do
					echo $(sorted_values $_val)
				done) | \
					tr ' ' '\n' |
					sort -u
			}

			function filtered_command_list() {
				# Horizontal to Vertical list (and sorted for join)
				function h2v() {
					tr ' ' '\n' <<< $@ | sort -u
				}

				function incl() {

					# OR:ed include-filter
					function ored() {
						grep -F -f <(h2v $filter_include)
					}

					# AND:ed include-filter
					function anded() {
						local icmd grep_cmd=""

						for icmd in $(h2v $filter_include); do
							grep_cmd="$grep_cmd | grep $icmd"
						done
						grep_cmd=$(sed -Ee 's/^ \| //' <<< $grep_cmd)
						eval $grep_cmd
					}

					if [ "X$capital_include" == "Xyes" ]; then
						ored
					else
						anded
					fi
				}

				function excl() {
					# AND:ed exclude-filter
					function anded() {
						grep -F -v -f <(h2v $filter_exclude)
					}

					# OR:ed exlude-filter. Grows with the difference of each
					# added list-element. This is the more difficult one to
					# understand as it's a negative or. It's however the most
					# useful: Exclude only those who's ANY categories are mentioned
					# in exlude-list
					function ored() {
						local icmd start_list=$(cat --)

						# Single-line command-list list to delimitered by <CR>
						function print_cmd_list() {
							echo $@ | sed -Ee 's/([^[:space:]]+ +:)/\n\1/g' | grep ":"
						}

						function pritty_print_cmd_list() {
							local _key _cmd_width=0

							# Find out width for nicer formatting
							for _key in $(print_cmd_list $@ | awk '{print $1}'); do
								if [ ${#_key} -gt $_cmd_width ]; then
									_cmd_width=${#_key}
								fi
							done

							print_cmd_list $@ | awk '{
								printf("%-'$_cmd_width's ", $1);
								for (i=2; i<=NF; i++){
									printf("%s ", $i);
								}
								printf("\n");
							}'
						}

						pritty_print_cmd_list $((for icmd in $(h2v $filter_exclude); do
							print_cmd_list $start_list | grep -F -v $icmd
						done) | sort -u) | sed -Ee 's/ $//'
					}

					if [ "X$capital_exclude" == "Xyes" ]; then
						ored
					else
						anded
					fi
				}

				if [ $filter_first == 'include' ]; then
					if [ "X$filter_exclude" == 'X' ]; then
						command_list | incl
					else
						command_list | incl | excl
					fi
				else
					if [ "X$filter_include" == 'X' ]; then
						command_list | excl
					else
						command_list | excl | incl
					fi
				fi
			}

			if [ "$show_cmd" == 'yes' ]; then
				if [ "X$use_filter" == 'Xyes' ]; then
					filtered_command_list
				else
					command_list
				fi
			else
				if [ "X$use_filter" == 'Xyes' ]; then
					filtered_command_list | awk '{print $1}'
				else
					core_list
				fi
			fi

		}

		if [ "$list_types" != 'yes' ]; then
			if [ "$include_internal" == 'yes' ]; then
				all
			else
				qualified
			fi
		else
			types
		fi

	}

	function process_one() {
		local _cmd=$1

		[ "X$show_cmd" == "Xyes" ] && echo $_cmd

		if [ "X$show_synopsis" == "Xyes" ]; then
			if [ "X$(type -t synopsis_${_cmd})" == "Xfunction" ]; then
				synopsis_${_cmd}
			fi
		fi

		if [ "X$show_usage" == "Xyes" ]; then
			if [ "X$(type -t usage_${_cmd})" == "Xfunction" ]; then
				usage_${_cmd}
			fi
		fi

		if [ "X$show_manpage" == "Xyes" ]; then
			if [ "X$(type -t help_${_cmd})" == "Xfunction" ]; then
				help_${_cmd}
			fi
		fi
	}

	function process() {
		local cmd

		if [ "X$list_all" == "Xyes" ]; then
			_fw_opts=""
			list_all='no'	## Prevent further recursion as this is global on all
								## levels below

			if [ "X$more_about" != "Xyes" ]; then
				list_commands
				return $?
			fi

			for cmd in $(list_commands); do
				process $cmd
			done
		else
			for cmd in $@; do
				process_one $cmd
			done
		fi
	}

	if [ $# -gt 0 ]; then
		if [ "$1" == "help" ] && [ "X$list_all" == "Xyes" ]; then
			process | indent | __ocdctrl_do_cmd ${@}
		else
			# Show help for command
			if [ "X$more_about" == "Xyes" ]; then
				# Break infinite recursion
				[[ $(nnested $FUNCNAME) > 3 ]] && return 0

				process ${@}
			else
				# Break infinite recursion
				[[ $(nnested $FUNCNAME) > 2 ]] && return 0

				# Fall-back on showing full manpage(s).
				__ocdctrl_help -m ${@}
			fi
		fi
	elif __ocdctrl_stdin_isapipe; then ##Note:  -eq 0 AND __stdion_isapipe
		while read -r; do
			for _cmd in $REPLY; do
				process_one $_cmd
         done
		done
	else
		if [ "X$more_about" == "Xyes" ]; then
			process | indent
		else
			if [ "X$list_all" != "Xyes" ]; then
				__ocdctrl_help help | indent ## No argumnets no opts, re-run with help to
											## self
			else
				process | indent
			fi
		fi
	fi

	return $_rc
}

if ! (return 0 2>/dev/null); then
	__ocdctrl_help "${@}"
	exit $?
fi
