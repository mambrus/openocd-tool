#!/bin/bash

shopt -s expand_aliases
alias LOGV='__alog V'
alias LOGD='__alog D'
alias LOGI='__alog I'
alias LOGW='__alog W'
alias LOGE='__alog E'
alias LOGC='__alog C'

readonly -A LOG_FUN=(
	[LOGV]='__alog V'
	[LOGD]='__alog D'
	[LOGI]='__alog I'
	[LOGW]='__alog W'
	[LOGE]='__alog E'
	[LOGC]='__alog C'
)
readonly -A LOG_PRFX=(
	[V]="VERBOSE"
	[D]="DEBUG"
	[I]="INFO"
	[W]="WARN"
	[E]="ERROR"
	[C]="CRITICAL"
)
readonly -A LOG_LEVEL=(
	[VERBOSE]=0
	[DEBUG]=1
	[INFO]=2
	[WARN]=3
	[ERROR]=4
	[CRITICAL]=5
)
# Translation table to sys-log equivalents
readonly -A LOG_SYSLOG_TAGS=(
	[V]="notice"
	[D]="debug"
	[I]="info"
	[W]="warning"
	[E]="err"
	[C]="crit"
)

readonly LOG_NLEVELS=${#LOG_LEVEL[@]}
readonly LOG_LEVEL_MAX=$((LOG_NLEVELS - 1 )) 	## Range: 0 - LOG_LEVEL_MAX
readonly LOG_QUIET=$((LOG_LEVEL_MAX + 1 ))

# Behaviour tables (defaults)
# - Append function name
LOG_FNAM=("no"      "yes"   "no"   "yes"  "yes"   "yes")
# - Print call-stack
LOG_TRAC=("no"      "no"    "no"   "no"   "yes"   "yes")

LOG_LEVEL_STDIO=${LOG_LEVEL_STDIO-${LOG_LEVEL[INFO]}}
LOG_LEVEL_SYSLOG=${LOG_LEVEL_SYSLOG-${LOG_LEVEL[VERBOSE]}}
LOG_SYSLOG_MSGID=${LOG_SYSLOG_MSGID-$(basename "$0")}

# Print log, LOG_LEVEL_STDIO permitting
# -------------------------------
# Content is accepted either as arguments or from stdin. If both are given,
# stdin is used for content and argument(s) are used for optionality.
#
# Args: If given, arg #1 is a one letter log-type: V, D, I, W, E, C for
#       VERBOSE, DEBUG, INFO, WARN, ERROR and CRITICAL respectively.
#
# Usage:
# __alog W "hello"                   ## Prints "hello" as WARN
# echo "hello" | __alog E            ## Prints "hello" as ERROR
# echo "hello" | __alog              ## Prints "hello" as VERBOSE
#
function __alog() {
	local _prev_rc=$?
	local _tag=$(tr '[:lower:]' '[:upper:]' <<< "${1}")
	local _line
	shift

	function printout() {
		local _tag=$1
		local _level=$(tag2lvl $1)
		shift


		if [ $_level -ge $LOG_LEVEL_STDIO ]; then
			echo -e "${LOG_PRFX[$_tag]}$(fname $_level):" "$@"
		fi
		if [ $_level -ge $LOG_LEVEL_SYSLOG ]; then
			logger --id=$$ -t ${LOG_SYSLOG_TAGS[$_tag]} -- \
				"${LOG_SYSLOG_MSGID}$(fname $_level):" "$@"
		fi
	}

	function print_callstack() {
		local _i
		local _nframes=${#FUNCNAME[@]}

		for (( _i=3; _i < _nframes; _i++ )); do
			printf "   %-15s:    %s +%s\n" \
				${FUNCNAME[$_i]} \
				${BASH_SOURCE[$_i]} \
				${BASH_LINENO[$_i]}
		done
	}

	function tag2lvl() {
		echo ${LOG_LEVEL[${LOG_PRFX[$1]}]}
	}

	# Conditionally output function-name
	function fname() {
		[ "X${LOG_FNAM[$1]}" == 'Xyes' ] && echo "(${FUNCNAME[4]})"
	}

	function logprint() {
		printout $_tag $@

		[ "X${LOG_TRAC[$_level]}" == 'Xyes' ] && print_callstack
	}

	[ "X$_tag" == "X" ] && _tag='v'

	if [ $# -ge 1 ]; then
		logprint $@ >&2
	else
		while read -r; do
			logprint $REPLY >&2
		done
	fi

	return $_prev_rc ## Return return-code it came in with to prevent
	                 ## clobbering
}

# Logged execution of external command
# ====================================

# Prefix any command with any of the 2 functins
# xlog - 4-way log
# qlog - 3-way log, i.e. "q" as in quit means don't
#                   also log stdout
#
# 4 way split works as follows
#
#                           : Defult         Setting
# ----------------------------------------------------
# -1 Command to be executed : LOGV           XLOG_TOX
# -2 Error if               : LOGE           XLOG_STDERR
# -3 stdout                 : LOGI           XLOG_STDOUT
# -4 stdout                 : stdout
#
# Note tah LOGS to stdout are slightly delayed compared with stdout
# itself. I.e. if terminated immediately after xlog, terminal will be
# slightly clobbered with delayed output.
#
function __xlog() {
   ${LOG_FUN[$XLOG_TOX]} "(exec:) $@"

	$1 ${@:2} 2> >(${LOG_FUN[$XLOG_STDERR]}) | \
      tee >(${LOG_FUN[$XLOG_STDOUT]})
	return ${PIPESTATUS[0]}
}

# As __ocdctrlsiadm() except that output is not logged
function __qlog() {
   ${LOG_FUN[$XLOG_TOX]} "(exec:) $@"

	$1 ${@:2} 2> >(${LOG_FUN[$XLOG_STDERR]})
	return ${PIPESTATUS[0]}
}
