#!/bin/bash

if ! (return 0 2>/dev/null); then
	source ocdctrl.sh ## Try source (if possible) to get the trampoline
	__ocdctrl_source_trampoline log
	__ocdctrl_source_trampoline misc
	__ocdctrl_source_trampoline getopts-fu
	__ocdctrl_source_ocdctrl
fi
ocdctrl_cmd[ocdctrl]='core framework user'

function syno_ocdctrl() {
	cat <<-EOF
	   ocdctrl
	EOF
}

function help_ocdctrl() {
	local _base_cmd=$(basename $0)

	cat <<-EOF
	NAME
	    ocdctrl

	SYNOPSIS

	$(syno_ocdctrl)

	DESCRIPTION
	   A user-friendly wrapper around OpenOCD

	   Features:
	   ---------
	      * Group OpenOCD settings in "projects"
	      * Easily manage running multiple OpenOCD sessions as services
	      * Each project is selectable by either
	        environment variable or one only option argument (-p)
	      * Multiple services can run on the same host easily distinguishable
	      * Plethora of interface, target and transport per project

	EXAMPLES
	    ocdctrl
	       Starts the gdbserver as a service

	    ocdctrl help -l
	       List sub-commands

	    ocdctrl <cmd> -h
	       Show help for sub-command

	OPERATION
	   This is a front-end controller to OpenOCD and gdbserver service manager.

	   The back-end are described in the OpenOCD "User's Guide":
	      http://OpenOCD.org/doc/pdf/OpenOCD.pdf

	OPTIONS
	    -h                 This help
	    -p NAME            Profile name
	    -x                 Flush command cache (devs only)

	   Logging:
	    -q                 Quench all fatals/errors/warnings/notifications
	    -l Level           Set stdio log-level
	    -L Level           Set syslog log-level

	   Level-value can be either a number, a case-insensitive string or letter.
	   Valid values for each value-type:
	       [0-$LOG_LEVEL_MAX]
	       [${!LOG_LEVEL[@]}]
	       [${!LOG_PRFX[@]}]

	ARGUMENTS
	   Argument is a sub-command. Commands can be passed either via stdin or
	   as argument.

	$(__ocdctrl_help -tc -j8)

	      List auto-generated using ocdctrl's introspection: ${_base_cmd/.sh/} help -tc -j8
	      More about introspection: ${_base_cmd/.sh/} help -h

	AUTHOR
	   Michael Ambrus <michael@helsinova.se>, Feruary 2020

	EOF
}

# Actuate  sys/stdio log-levels as per option
# to ignore log-levels in settings if corresponding option is set
function optactuate_loglevels(){
	[ -v _log_level_stdio ] && LOG_LEVEL_STDIO=$_log_level_stdio
	[ -v _log_level_up ] && LOG_LEVEL_STDIO=$(( LOG_LEVEL_STDIO - _log_level_up  ))
	if [ $LOG_LEVEL_STDIO -lt 0 ]; then
		LOG_LEVEL_STDIO=0
	fi
	[ -v _log_level_syslog ] && LOG_LEVEL_SYSLOG=$_log_level_syslog
}

function getopts_ocdctrl() {

	# Make a valid loglevel from input which can be either numerical
	# text or letter. If non-numerical, lupper/locaer case are treated
	# the same
	function opt2loglevel() {
		local _regex='[[:digit:]]+'
		local _level=-1
		local -u _arg=$1 ## Assign and upper-case argument

		if [[ $_arg =~ $_regex ]]; then
			_level=$_arg
		else
			# Look-up differently depending single-letter or string
			if [ ${#_arg} -gt 1 ]; then
				_level=${LOG_LEVEL[$_arg]}
			else
				_level=${LOG_LEVEL[${LOG_PRFX[$_arg]}]}
			fi
		fi

		if [ $_level -ge 0 ] && [ $_level -le $LOG_LEVEL_MAX ]; then
			echo $_level
			return 0
		fi

		LOGC "Invalid option argument (-L/-s): [$_arg]"
		return 1
	}

	while getopts $_options OPTION; do
		case $OPTION in
		p)
			_profile_name=$OPTARG
			;;
		l)
			_log_level_stdio=$(opt2loglevel $OPTARG) || exit 1
			optactuate_loglevels ## Actuate early to take effect during parsing
			;;
		L)
			_log_level_syslog=$(opt2loglevel $OPTARG) || exit 1
			optactuate_loglevels ## Actuate early to take effect during parsing
			;;
		x)
			rm -f /tmp/ocdctrl.corelist
			__ocdctrl_source_ocdctrl force
			;;
		q)
			_log_level_stdio=$LOG_QUIET
			;;
		esac
	done
	OPTIND=1

	return $?
}

function __ocdctrl_ocdctrl() {
	# pick up .local.env (if possible) to get OCDCTRL_PROJECT name
	# This enables option-less operation where applicable
	eval $(_env show env_local)
	eval $(_env show constants)      ## Version gets set here
	GETOPTSFU_VERSION=$OCDCTRL_VERSION   ## Set generic variable to project
                                    ## specific
	local _profile_name
	local _log_level_stdio
	local _log_level_syslog
	local _log_level_up
	local _relaxed_cmds              ## Don't require a profile

	_getopts_begin p:l:L:xq help_ocdctrl
		getopts_ocdctrl "${@}"
	_getopts_end "${@}"
	shift $(($OPTIND - 1))

	_profile_name=${_profile_name-${OCDCTRL_PROJECT}}
	OCDCTRL_PROJECT=${OCDCTRL_PROJECT-${_profile_name}}
	_relaxed_cmds=$(__ocdctrl_help -t -i core -i framework -I)

	if __help_not_requested "${@}"; then
		if [ "X${_profile_name}" == "X" ] && ! [[ $_relaxed_cmds == *"$1"* ]]; then
			LOGE "Project unknown\n set environment variable or use option"
			exit 1
		fi
	fi

	if [ "X$_profile_name" != 'X' ]; then
		load_environment $_profile_name
		OCDCTRL_PROFILE_FIRST=$_profile_name
		optactuate_loglevels ## Re-actuate loglevels from optitions despite
                           ## (i.e. default) settings from environment
	fi

	__ocdctrl_do_cmd "${@}"
	return $?
}

if ! (return 0 2>/dev/null); then
	__ocdctrl_ocdctrl "${@}"
	exit $?
fi
