#!/bin/bash

if ! (return 0 2>/dev/null); then
	source ocdctrl.sh ## Try source (if possible) to get the trampoline
	__ocdctrl_source_trampoline log
	__ocdctrl_source_trampoline misc
	__ocdctrl_source_trampoline getopts-fu
	__ocdctrl_source_ocdctrl
fi
ocdctrl_cmd[log]='framework core'

function synopsis_log() {
	cat <<-EOF
	log <[OPTION]> <String>
	EOF
}

function usage_log() {
	cat <<-EOF
	log [-l | [OPTION]... [COMMAND]...]
	EOF
}

function help_log() {
	cat <<-EOF
	NAME
	    ocdctrl-log

	SYNOPSIS
	    $(synopsis_log)

	DESCRIPTION
	    Log input using ocdctrl's logging sub-system

	EXAMPLES
	    ocdctrl -L info log "hello world"
	        Sets log-level to permit INFO logs, then log "hello word"
	    echo "hello" | ocdctrl -L info log -e
	        Log "hello" as an error

	OPERATION
	    Conditionally print log-string on stdio/syslog depending on
	    values global environment values LOG_LEVEL_STDIO/LOG_LEVEL_SYSLOG

	    If log passes filters, it will be tagged according to it's
	    category, i.e. [${!LOG_LEVEL[@]}]

	    The input can be either argument(s) or comming from stdin

	OPTIONS
	    -h                 This help
	    -v                 Log input as VERBOSE
	    -d                 Log input as DEBUG
	    -i                 Log input as INFO
	    -w                 Log input as WARN
	    -e                 Log input as ERROR
	    -c                 Log input as CRITICAL

	ARGUMENTS
	   One or several log-strings

	AUTHOR
	   Michael Ambrus <michael@helsinova.se>, July 2019

	EOF
}

function getopts_log() {
	local _rc=0
	local _tag='I'

	while getopts $_options OPTION; do
		case $OPTION in
		v)
			_tag='V'
			;;
		d)
			_tag='D'
			;;
		i)
			_tag='I'
			;;
		w)
			_tag='W'
			;;
		e)
			_tag='E'
			;;
		c)
			_tag='C'
			;;
		esac
	done
	OPTIND=1

	echo local _tag="$_tag"

	return $_rc
}

function __ocdctrl_log() {
	_getopts_begin vdiwec help_log
		eval $(getopts_log "${@}")
	_getopts_end "${@}"
	shift $(($OPTIND - 1))

	__alog $_tag $@

	return $?
}

if ! (return 0 2>/dev/null); then
	__ocdctrl_log "${@}"
	exit $?
fi
