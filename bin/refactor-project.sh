#! /bin/bash
# Refactor all commands and command-name files and internal references
# Arg1: from name
# Arg2: to name

if ! (return 0 2>/dev/null); then
	BASE="$(dirname $(readlink -f $0))"
	source "$BASE/../lib/log.sh"
	source "$BASE/../lib/isc.sh"
	source "$BASE/../lib/getopts-fu.sh"
fi

function synopsis_refactor_project() {
	cat <<-EOF
	refactor_project <[OPTION]> <String>
	EOF
}

function usage_refactor_project() {
	cat <<-EOF
	refactor_project [-l | [OPTION]... [COMMAND]...]
	EOF
}

function help_refactor_project() {
	cat <<-EOF
	NAME
	    refactor-project

	SYNOPSIS
	    $(synopsis_refactor_project)

	DESCRIPTION
	    Refactors a project from one name into another

	EXAMPLES

	OPERATION

	OPTIONS
	    -h                 This help

	ARGUMENTS
	   Arg1: <from-name>
	   Arg2: <to-name>

	AUTHOR
	   Michael Ambrus <michael@helsinova.se>, Sept 2019

	EOF
}

function getopts_refactor_project() {
	OPTIND=1

	while getopts $_options OPTION; do
		case $OPTION in
		m)
			_opmode=$OPTARG
			;;
		esac
	done

	return $_rc
}

function __refactor_project() {
	local _opmode

	_getopts_begin m: help_refactor_project
		getopts_refactor_project "${@}"
	_getopts_end "${@}"
	shift $(($OPTIND - 1))

	local _src=$1
	local _dst=$2
	local -u _usrc=$1
	local -u _udst=$2

	if ! [ -d '.git' ]; then
		LOGE "Not in root directory"
		exit 1
	fi

	function git_cmd() {
		LOGV "git $@"

		git $@ 2> >(LOGE) | tee >(LOGI)
		return ${PIPESTATUS[0]}
	}


	function rename_files_in_dir() {
		LOGI "Renaming files [$_src -> $_dst] in directory: [$1]"

		# Does what the rename command does, but involves git too
		(
			local _ifile _ofile
			cd $1

			for _ifile in $(ls *$_src*); do
				_ofile=$(sed -Ee "s/$_src/$_dst/g" <<< $_ifile)
				LOGI "  $_ifile -> $_ofile"
				git_cmd mv $_ifile $_ofile || exit $?
			done
		)
	}

	function searc_replace_glob() {
		shopt -s extglob ## Enable extended bash globbing
		local _file _files

		function search_replace() {
			local _src=$1
			local _dst=$2

			_files=`grep -R $_src +(*|.env|.gitignore|.vim.cutom) | cut -f1 -d':' | uniq`
			for _file in $_files; do
				LOGI "S&R: [$_src -> $_dst] processing [$_file]"
				sed -i "s/$_src/$_dst/g" $_file
			done
		}
		search_replace $_src $_dst
		search_replace $_usrc $_udst

	}

	rename_files_in_dir '.'
	rename_files_in_dir 'bin'
	rename_files_in_dir 'lib'

	searc_replace_glob
}

if ! (return 0 2>/dev/null); then
	__refactor_project "$@"
fi
