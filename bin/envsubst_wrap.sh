#! /bin/bash
# Helper wrapper to be used when envsubst can't be used "as is"

if ! (return 0 2>/dev/null); then
	BASE="$(dirname $(readlink -f $0))"
	source "$BASE/../lib/log.sh"
	source "$BASE/../lib/misc.sh"
	source "$BASE/../lib/getopts-fu.sh"
fi

function synopsis_envsubst_wrap() {
	cat <<-EOF
	Wrap GNU coreutil's "envsubst"
	EOF
}

function usage_envsubst_wrap() {
	cat <<-EOF
	envsubst_wrap [OPTION] [INFILE [OUTFILE]]
	EOF
}

function help_envsubst_wrap() {
	cat <<-EOF
	NAME
	    envsubst-wrap

	SYNOPSIS
	    $(synopsis_envsubst_wrap)

	DESCRIPTION
	    Run envsubst from environment set up from options

	OPTIONS
	    -h                 This help
	    -e STRING          Environment variale declaration in STRING in
	                       fomat VAR="word word..."
	                       Option given per variable.

	AUTHOR
	   Michael Ambrus <michael@ambrus.se>, May 2021

	EOF
}

function getopts_envsubst_wrap() {
	OPTIND=1
	local _re='([^[:space:]]+)(=)(.*)'

	while getopts $_options OPTION; do
		case $OPTION in
		e)
			if [[ $OPTARG =~ $_re ]]; then
				if ! [ "${_variable[${BASH_REMATCH[1]}]}" ]; then
					_declaration[${#_declaration[@]}]=$(
						echo $OPTARG | sed -E \
							-e 's/^[[:space:]]*//' \
							-e 's/[[:space:]]*$//'
					)
					_variable[${BASH_REMATCH[1]}]=$(
						echo ${BASH_REMATCH[3]} | sed -E \
							-e 's/^[[:space:]]*//' \
							-e 's/[[:space:]]*$//'
				)
				else
					LOGE "Variable declaration already given for [$OPTARG]." \
						" Previous value: [${_variable[${BASH_REMATCH[1]}]}]"
					LOGE "Re-run $(basename $0) with -h for help"
					exit 1
				fi
			else
				LOGE "Environment variable declaration syntax: " \
					"[$OPTARG]"
				LOGE "Re-run $(basename $0) with -h for help"
				exit 1
			fi

			;;
		esac
	done

	return $_rc
}

function __envsubst_wrap() {
	local -a _declaration
	local -A _variable
	local -a _file
	local _shell_format

	_getopts_begin e: help_envsubst_wrap
		getopts_envsubst_wrap "${@}"
	_getopts_end "${@}"
	shift $(($OPTIND - 1))

	LOGD ""
	#LOGD "$(declare -p _declaration)"
	#LOGD "$(declare -p _variable)"

	eval export ${_declaration[@]}

	_file[0]='--'
	_file[1]='--'

	#
	# Test if arg[2] is a file and assign to _file[arg[1]] if so.
	# Boolean arg[3] decides if file existence is required
	#
	function filename_assign() {
		if [ $3 -eq 1 ]; then
			if ! [ -f "$2" ]; then
				LOGE "File not found: $2"
				return 1
			fi
		fi
		_file[ $(( $1 - 1 )) ]="$2"
		return 0
	}

	if [ $# -gt 2 ]; then
		LOGE "Too many arguments: $#"
		return 1
	fi
	if [ $# -ge 1 ]; then
		filename_assign 1 "$1" 1 || return 1
	fi
	if [ $# -eq 2 ]; then
		filename_assign 2 "$2" 0 || return 1
	fi
	LOGD "$(declare -p _file)"

	if [ ${#_variable[@]} -gt 0 ]; then
		_shell_format="$(echo ${!_variable[@]} | sed -e 's/ /\n/g' | sed -e 's/^/$/')"
		LOGD "${!_variable[@]}"
	fi
	LOGD "${_shell_format}"

	cat ${_file[0]} | envsubst "$_shell_format" | (
		if [ "${_file[1]}" == "--" ]; then
			cat --
		else
			cat -- > "${_file[1]}"
		fi
	)
}

if ! (return 0 2>/dev/null); then
	__envsubst_wrap "$@"
fi
