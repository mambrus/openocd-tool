#! /bin/bash

#
# Expand arg as either function, alias or exec
#
_expand_exec() {
    expand_name() {
        local name

        # Is it a function
        declare -F "$1" && return 0

        # If it's an alias, expand it fully. Arguments, options and all
        if name=$(type "$1" 2>/dev/null); then
            [[ "$name" == *"aliased"* ]] && sed -e 's/.*`//' -e 's/'\''$//' <<< "$name" && return 0
        fi

        # Out of options - default to what must be loadable executable
        echo "$1"; return 0
    }
    expand_name "$1"
}
#
# Author: Brian Beffa <brbsix@gmail.com>
# Original source: https://brbsix.github.io/2015/11/29/accessing-tab-completion-programmatically-in-bash/
# License: LGPLv3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
#
get_completions(){
    local completion COMP_CWORD COMP_LINE COMP_POINT
    local -a COMP_WORDS COMPREPLY=()

    # load bash-completion if necessary
    declare -F _completion_loader &>/dev/null || {
        source /usr/share/bash-completion/bash_completion
    }

    # replace any occurrence of magic marker at EOL with ONE space
    COMP_LINE=$(sed -Ee 's/[[:space:]]*£+$/ /' <<< $*)
    COMP_POINT=${#COMP_LINE}

    eval set -- "$@"

    COMP_WORDS=("$@")

    # align with the corresponding treatment as COMP_LINE (EOL magic-marker)
    COMP_WORDS[-1]=$(sed -Ee 's/£+$//' <<< ${COMP_WORDS[-1]})
    [[ ${COMP_WORDS[-1]} == '' ]] &&  unset COMP_WORDS[-1]

    # execute-in-place if EOL marker is foud
    if [[ ${COMP_LINE[@]: -1} = '±' ]]; then
        COMP_WORDS[-1]=$(sed -Ee 's/[[:space:]]*±+$//' <<< ${COMP_WORDS[-1]})
        [[ ${COMP_WORDS[-1]} == '' ]] &&  unset COMP_WORDS[-1]
        $(_expand_exec ${COMP_WORDS[0]}) "${COMP_WORDS[@]:1}"
        return $?
    fi

    # add '' to COMP_WORDS if the last character of the command line is a space
    [[ ${COMP_LINE[@]: -1} = ' ' ]] && COMP_WORDS+=('')

    # index of the last word
    COMP_CWORD=$(( ${#COMP_WORDS[@]} - 1 ))

    # determine completion function
    completion=$(complete -p "$1" 2>/dev/null | awk '{print $(NF-1)}')

    # run _completion_loader only if necessary
    [[ -n $completion ]] || {

        # load completion
        _completion_loader "$1"

        # detect completion
        completion=$(complete -p "$1" 2>/dev/null | awk '{print $(NF-1)}')

    }

    # ensure completion was detected
    [[ -n $completion ]] || return 1

    # execute completion function
    "$completion"

    # print completions to stdout
    printf '%s\n' "${COMPREPLY[@]}" | LC_ALL=C sort
}
#
# ###### Barrier ######
# This file is operating differently depending if sourced or not.
#
# The get_completion() function can work in a sub-shell, the rest can't.
#
if ! (return 0 2>/dev/null); then
    get_completions "$@"
    exit $?
fi
#
# Auto complete function
#
_get_completions(){
    COMPREPLY=()
    local _cur=${COMP_WORDS[COMP_CWORD]}
    local _COMP_LINE
    local -a _COMP_WORDS

    _COMP_WORDS=("${COMP_WORDS[@]:1}")
    _COMP_LINE="${_COMP_WORDS[@]}"
    #
    # White-spaces in COMP_LINE must NOT be removed, hence the need to
    # look-up in temporary variable first
    #
    local _sugst=$(get_completions "${_COMP_LINE}")
    COMPREPLY=($(compgen -W "$_sugst" -- "$_cur"))
}

export -f get_completions
export -f _get_completions
export -f _expand_exec
complete -F _get_completions get_completions
