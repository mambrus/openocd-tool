#! /bin/bash
# Generate command-source from template file
#
# Input is a 1..n list of command-names
# Input is take taken either from stdin or as arguments
#
# Note:
# Input management is handled not in this script but by the generic
# input management function __ocdctrl_handle_input

if ! (return 0 2>/dev/null); then
	BASE="$(dirname $(readlink -f $0))"
	source "$BASE/../lib/log.sh"
	source "$BASE/../lib/misc.sh"
fi

TEMPLATE_FILE=${TEMPLATE_FILE-"ocdctrl-template.sh"}
TEMPLATE_SNAME=${TEMPLATE_SNAME-"template"}
if ! [ -v DEST_DIR ]; then
	DEST_DIR=$(cd $(dirname $(readlink -f $0))/../lib; pwd)
fi

function generate_one() {
	local _outfile="$DEST_DIR/ocdctrl-$(tr '[:upper:]' '[:lower:]' <<< $1).sh"

	function mk_fname() {
		tr '-' '_' <<< $1 | tr '[:upper:]' '[:lower:]'
	}

	local _fname=$(mk_fname $1)
	LOGI "Generate source for command [$_fname] as [$_outfile]"
	if ! [ -f "$TEMPLATE_FILE" ]; then
		LOGE "No template-file: $TEMPLATE_FILE"
		exit 1
	fi
	cp "$TEMPLATE_FILE" $_outfile
	sed -i "s/${TEMPLATE_SNAME}/$_fname/g" $_outfile
}

if ! (return 0 2>/dev/null); then
	# Input list either from arguments or from stdin. Th -a option is adhered
	# only if neither is given
	rm -f /tmp/ocdctrl.corelist
	__ocdctrl_handle_input -a "ocdctrl-dummy" -f generate_one -- $@
fi
