#!/bin/bash
# Core-shell for ocdctrl
#
# Contains trampolines and the first entry-point for a command
# interpretor.

if ! (return 0 2>/dev/null); then
	ENV_BASE=${ENV_BASE-"${INSTALL_DIR_LIB}"} ## Where settings/env templates are
	source "$(readlink -f $0)"
	__ocdctrl_source_trampoline log
	__ocdctrl_source_trampoline misc
	__ocdctrl_source_trampoline getopts-fu
	__ocdctrl_source_ocdctrl
fi

# Source trampoline. Finds sub-commands of function libraries by well-known
# path and extensions and sources them by full path. This takes away the
# possibility of sourcing the wrong file just because it's in PATH, or the
# need of having it in PATH at all, effectively clobbering the
# system.installation. Side-effect is hardening security.
function __ocdctrl_source_trampoline() {
	local _this_base="$(dirname $(readlink -f $0))"

	# Consider devs first
	if ! [[ $_this_base =~ ^/usr/* ]]; then
		# Look side-by-side
		local _the_file="$_this_base/${1}"
		if [ -f "$_the_file" ]; then
			source "$_the_file" "${@:2}"
			return 0
		fi
		local _the_file="${_the_file}.sh"
		if [ -f "$_the_file" ]; then
			source "$_the_file" "${@:2}"
			return 0
		fi

		# Executed from source repository libs and mains can source each other.
		# Extension is known here, i.e no need to try both.
		local _the_file="$_this_base/../bin/${1}.sh"
		if [ -f "$_the_file" ]; then
			source "$_the_file" "${@:2}"
			return 0
		fi
		local _the_file="$_this_base/../lib/${1}.sh"
		if [ -f "$_the_file" ]; then
			source "$_the_file" "${@:2}"
			return 0
		fi
	fi

	# Fall-back on installation directories
	# Extension is known here, i.e no need to try both.
	local _the_file="$INSTALL_DIR_BIN/${1}"
	if [ -f "$_the_file" ]; then
		source "$_the_file" "${@:2}"
		return 0
	fi
	local _the_file="$INSTALL_DIR_LIB/${1}"
	if [ -f "$_the_file" ]; then
		source "$_the_file" "${@:2}"
		return 0
	fi

	echo "System error - Source not found: $1" >&2
	exit 2
}

# Similar to "source_trampoline", except that it searches and includes
# all shell-script's with the key-word "ocdctrl-", i.e. sub-commands.
# all in one go.
function __ocdctrl_source_ocdctrl() {
	local _this_base="$(dirname $(readlink -f $0))"
	local _arg=$1

	declare -Ag ocdctrl_cmd       ## Assosiative array to be set by the sourced
                              ## commands.

	# Since source-scripts can be spread out and since there can
	# exist both installed versions and development versions, this is a little
	# quirky. I.e. multiple includes need to be detected and order is relevant.
	function source_from() {
		local _dir="${1}"
		local _files=$(ls $_dir/ocdctrl-* 2>/dev/null)

		for F in $_files; do
			local _cmd=$(basename $F | sed -Ee 's/\.sh$//' -e 's/ocdctrl-//')
			#LOGD "Looking for command $_cmd"

			if [ "X${_arg}" == 'Xforce' ] ||
				[ ! "X$(type -t __ocdctrl_${_cmd})" == "Xfunction" ];
			then
				#LOGV "Considering: $F"
				source $F
				echo "source $F" >> /tmp/ocdctrl.corelist
			else
				LOGW "Command $_cmd already sourced.\n Skipping: $F"
			fi
		done
	}

	if [ -f /tmp/ocdctrl.corelist ]; then
		source /tmp/ocdctrl.corelist
		return $@
	fi

	# Look side-by this first and if dev, ext *.sh. Installed sources installed
	# first
	source_from $_this_base

	source_from $_this_base/../bin ## This might be overkill
	source_from $_this_base/../lib ## This isn't overkill
	source_from $INSTALL_DIR_BIN
	source_from $INSTALL_DIR_LIB
}

if ! (return 0 2>/dev/null); then
	__ocdctrl_ocdctrl "${@}"
	exit $?
fi
