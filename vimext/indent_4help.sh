#! /bin/bash
# Vim source filter
#
# Replace all type of indent-spaces in buffer with spaces
# Special version for help-sections

if ! (return 0 2>/dev/null); then
	BASE="$(dirname $(readlink -f $0))"
	source "$BASE/../lib/log.sh"
	source "$BASE/../lib/misc.sh"
fi

function process_file() {
	local _tabstop=$1 ## Number of spaces per tabstop chunk
	local _space_chunk="" ## Chunk build-up
	local _loop

	for (( _loop=0; _loop<$_tabstop; _loop++)); do
		_space_chunk="$_space_chunk "
	done

	function process_line() {
		local _line=$@
		local _code
		local _spaces
		local _new_spaces

		if [[ $_line =~ ^[[:space:]] ]]; then
			_code=$(echo "$_line" | sed -Ee 's/^([[:space:]]+)(.*)/\2/')
			_spaces=$(echo "$_line" | sed -Ee 's/^([[:space:]]+)(.*)/\1/')
			_new_spaces=$(sed -Ee "s/\t/$_space_chunk/g" <<< $_spaces) ## Replace all to space whites
			_new_spaces=$(sed -Ee "s/^$_space_chunk/\t/" <<< $_new_spaces) ## Replace only first whie-chunk

			echo "${_new_spaces}${_code}"
		else
			echo "$_line"
		fi
	}

	if ! [ -t 0 ]; then
		while read -r; do
			process_line "$REPLY" | sed -Ee 's/^[[:space:]]+$//'
		done
	else
		LOGE "Stdin is not a pipe"
	fi
}

if ! (return 0 2>/dev/null); then
	LOGD "process_file [$@]"
	process_file "$@"
fi

