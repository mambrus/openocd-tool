#! /bin/bash

#
# List completion functions for this project currently loaded in
# environment
#
function list_current() {
    for F in $(
        declare -F | \
            grep _ocdctrl | \
            grep -v __ | \
            awk '{print $3}');
    do
        echo $F;
    done
}
#
# Decorate functions as command
#
function cmd_current() {
    for F in $(list_current); do
        echo "export -f $F"
    done
}

if ! (return 0 2>/dev/null); then
    echo "Error: You need to source this file!" >&2
    exit 1
fi

declare -i numberof=$(list_current | wc -l)
if [ $numberof -eq 0 ]; then
    echo "*** No functions to export. Perhaps you need to source" \
        "some completion files?" >&2
else
    eval "$(cmd_current)" && \
        echo "Successfully exported functions: $numberof" || \
        ( echo "*** Unknown error. Failed exporting functions: $numberof" >&2 )
fi

unset numberof


