#!/bin/bash

OCDCTRL_COMPLETION_LOGFILE=${OCDCTRL_COMPLETION_LOGFILE-"/tmp/ocdctrl_completion.log"}
OCDCTRL_COMPLETION_PLOGFILE=${OCDCTRL_COMPLETION_PLOGFILE-"/tmp/ocdctrl_completion_pipe.log"}
# TODO: Change following default to "no"
OCDCTRL_COMPLETION_DOLOG=${OCDCTRL_COMPLETION_DOLOG-"yes"}

declare -a -g OCDCTRL_SUBCMD=()
declare -A -g OCDCTRL_SUBCMD_IDX=()
declare -a -g OCDCTRL_OPTS=()
declare -A -g OCDCTRL_OPTS_IDX=()
declare -A -g OCDCTRL_OPTS_ARG=()

#
# List options for a sub-command
# General helper utilizing sub-command help-pages
#
function _ocdctrl_completion_optfor() {
    ocdctrl $1 -h | \
        sed -ne "/OPTIONS/,/ARGUMENTS/P" | \
        grep -Ee '[[:space:]]-' | \
        sed -Ee 's/^[[:space:]]*//' | \
        grep -Ee '^-' | \
        awk '{print $1}'
}
#
# List options for a sub-command including opt-args
# General helper utilizing sub-command help-pages
#
# Note: 1) Format dependent. Help-text must start beyond assumed column
#       2) Output format is adjusted for exactly one space between the letter
#          and any following opt-arg text
#
function _ocdctrl_completion_optarg_for() {
    ocdctrl $1 -h | \
        sed -ne "/OPTIONS/,/ARGUMENTS/P" | \
        grep -Ee '[[:space:]]-' | \
        sed -Ee 's/^[[:space:]]*//' | \
        grep -Ee '^-' | \
        cut -c-15 | \
        sed -Ee 's/[[:space:]]*$//' | \
        sed -Ee 's/(..)(.*)/\1 \2/' | \
        awk '{print $1" "$2}'
}
#
# Count valid commands in pipe
#
# Use for determining command depth-level.
#
# Distinguishing from options, opt-args and non command args is done
# by utilizing that only commands have a help. I.e. Matching against
# OCDCTRL_SUBCMD_IDX serves the purpose.
#
function _ocdctrl_completion_cmd_depth() {
    local -i _j=${COMP_CWORD}
    local -i _i
    local -i _k=0
    local _tcmd

    for (( _i=0; _i < $_j; _i++ )); do
        if [ "X${COMP_WORDS[$_i]}" != "X" ]; then
            if [[ ${COMP_WORDS[$_i]:0:1} != "-" ]]; then
                _tcmd="${COMP_WORDS[$_i]}"
                _tcmd=$(_ocdctrl_completion_uniform_cmd $_tcmd)
                if [ "${OCDCTRL_SUBCMD_IDX[$_tcmd]}" ]; then
                    (( _k++ ))
                fi
            fi
        fi
    done
    echo $_k
}
#
# Return 1:st word in COMP_WORDS that also exists in _from_array
#
function _ocdctrl_completion_first_from() {
    local -i _i
    local -i _j=${COMP_CWORD}
    local -i _k
    local _from_array=("$@")

    for (( _i=0; _i < $_j; _i++ )); do
        if [ "X${COMP_WORDS[$_i]}" != "X" ]; then
            for _k in ${!_from_array[@]}; do
                if [ "${COMP_WORDS[$_i]}" == "${_from_array[$_k]}" ]; then
                    echo "${COMP_WORDS[$_i]}"
                    return 0
                fi
            done
        fi
    done
    return 1
}
#
# Return 1:st word in COMP_WORDS *after* specific word that also exists in
# _from_array. This search is useful when keywords are valid both as
# sub-commands for the back-end and the front-end
#
# Arguments
#  1: skip_to
#  2: from_array
#
function _ocdctrl_completion_first_from_after() {
    local -i _i
    local -i _j=${COMP_CWORD}
    local -i _k
    local _skip_to=$1
    shift
    local _from_array=("$@")

    # Skip-phase
    function skip() {
        for (( _i=0; _i < $_j; _i++ )); do
            if [ "X${COMP_WORDS[$_i]}" == "X${_skip_to}" ]; then
                return 0
            fi
        done
        return 1
    }

    skip || return 1
    _ocdctrl_completion_log "Skipped to [$_i]: ${COMP_WORDS[$_i]}"

    # Search rest
    for (( _i++ ; _i < $_j; _i++ )); do
        if [ "X${COMP_WORDS[$_i]}" != "X" ]; then
            for _k in ${!_from_array[@]}; do
                if [ "${COMP_WORDS[$_i]}" == "${_from_array[$_k]}" ]; then
                    _ocdctrl_completion_log "Found at [$_i]: ${COMP_WORDS[$_i]}"
                    echo "${COMP_WORDS[$_i]}"
                    return 0
                fi
            done
        fi
    done
    _ocdctrl_completion_log "Not found until end [$_i]"
    return 1
}
#
# Like function _ocdctrl_completion_first_from_after() but instead of
# returning 1:st word, it skips that too and returns the rest
#
# Arguments
#  1: skip_to
#  2: from_array
#
function _ocdctrl_completion_thus_far() {
    local -i _i
    local -i _j=${COMP_CWORD}
    local -i _k
    local _skip_to=$1
    shift
    local _from_array=("$@")

    # Skip-phase 1
    function skip() {
        for (( _i=0; _i < $_j; _i++ )); do
            if [ "X${COMP_WORDS[$_i]}" == "X${_skip_to}" ]; then
                return 0
            fi
        done
        return 1
    }

    skip || return 1
    _ocdctrl_completion_log "Skipped 1 to [$_i]: ${COMP_WORDS[$_i]}"

    # Skip-phase 2
    function skip2() {
        for (( _i++ ; _i < $_j; _i++ )); do
            if [ "X${COMP_WORDS[$_i]}" != "X" ]; then
                for _k in ${!_from_array[@]}; do
                    if [ "${COMP_WORDS[$_i]}" == "${_from_array[$_k]}" ]; then
                        return 0
                    fi
                done
            fi
        done
        return 1
    }

    skip2 || return 1
    _ocdctrl_completion_log "Skipped 2 to [$_i]: ${COMP_WORDS[$_i]}"

    for (( _i++ ; _i < $_j; _i++ )); do
        if [ "X${COMP_WORDS[$_i]}" != "X" ]; then
            _ocdctrl_completion_log "Thus far [$_i]: ${COMP_WORDS[$_i]}"
            echo "${COMP_WORDS[$_i]}"
        fi
    done
    return 0
}
#
# Optionable logging
#
function _ocdctrl_completion_log() {
    if [ $OCDCTRL_COMPLETION_DOLOG == "yes" ]; then
        echo "$@"  >> ${OCDCTRL_COMPLETION_LOGFILE}
    fi
}
#
# Optionable pipe logging
#
# Arg1: Log decorator
#
function _ocdctrl_completion_plog() {

    function logit() {
        if [ $# -gt 0 ]; then
            local _decos="${1}: "
            local -i _ncut=${#_decos}

            sed -Ee 's/^/'"$_decos"'/' | \
                tee -a ${OCDCTRL_COMPLETION_PLOGFILE} | \
                cut -c${_ncut}-
        else
            tee -a ${OCDCTRL_COMPLETION_PLOGFILE}
        fi
    }

    if [ $OCDCTRL_COMPLETION_DOLOG == "yes" ]; then
        logit $@
    else
        cat--
    fi
}
#
# Get last non-option (i.e. arg) from current COMP_WORDS
#
# Note: Word preceded by an opt taking a opt-arg will be returned as a
#       non-opt which is a flaw (FIXME TODO)
#
function _ocdctrl_completion_last_nonopt() {
    local -i _i
    local _targ

    for (( _i=$COMP_CWORD; _i >= 0; _i-- )); do
        if [ "X${COMP_WORDS[$_i]}" != "X" ]; then
            if [[ ${COMP_WORDS[$_i]:0:1} != "-" ]]; then
                echo "${COMP_WORDS[$_i]}"
                return 0
            fi
        fi
    done
    return 1
}
#
# Get last command from current COMP_WORDS
#
function _ocdctrl_completion_last_cmd() {
    local -i _i
    local _tcmd

    for (( _i=$COMP_CWORD; _i >= 0; _i-- )); do
        if [ "X${COMP_WORDS[$_i]}" != "X" ]; then
            if [[ ${COMP_WORDS[$_i]:0:1} != "-" ]]; then
                _tcmd="${COMP_WORDS[$_i]}"
                _tcmd=$(_ocdctrl_completion_uniform_cmd $_tcmd)
                if [ "${OCDCTRL_SUBCMD_IDX[$_tcmd]}" ]; then
                    echo ${_tcmd}
                    return 0
                fi
            fi
        fi
    done
    return 1
}
#
# Uniform command
#  * Aliased, get the alias
#  * If suffixed, strip it
#  * If function prefixed, strip it
#
function _ocdctrl_completion_uniform_cmd() {
    local _cmd=$1

    # If aliased, dig out the actual command (strip any tailing arguments
    # and options)
    if alias $_cmd >/dev/null 2>/dev/null; then
        _cmd=$(alias $_cmd | \
            cut -f2 -d"=" | \
            awk '{print $1}' | \
            sed -Ee "s/^'//"  -e 's/'"'"'$//'
        )
    fi
    _cmd=$(sed -Ee 's/\.sh$//' <<< $_cmd)
    _cmd=$(basename $_cmd)
    _cmd=$(sed -Ee 's/^__//' <<< $_cmd)
    echo "${_cmd}"
}
#
# Dynamically load completion for sub-command if needed
#
function _ocdctrl_completion_loader() {
    local _cmd=$1

    _ocdctrl_completion_log "Auto-load completions for: [$_cmd]" \
        "(_ocdctrl_completion_${_cmd})"

    # Early exit if no need to load
    $(complete -p ocdctrl-${_cmd} 2>/dev/null) && return 0
    _ocdctrl_completion_log "Completion for [$_cmd] needed (1)"

    local _filename
    #
    # Try devs
    local _base
    [[ "X${PROJECT}"=="Xocdctrl" ]] && [[ _base="$(dirname ocdctrl.sh)" ]] && \
        [[ -x "$_base"/completion/_ocdctrl-${_cmd}-completion ]] && \
            source "$_base"/completion/_ocdctrl-${_cmd}-completion || \
            _ocdctrl_completion_log "Skip 1:" \
            "$_base"/completion/_ocdctrl-${_cmd}-completion

    $(complete -p ocdctrl-${_cmd} 2>/dev/null) && return 0
    _ocdctrl_completion_log "Completion for [$_cmd] needed (2)"

    #
    # Try /usr/local/share
    _filename="/usr/local/share/ocdctrl/completion/ocdctrl-${_cmd}-completion"
    [[ -f "$_filename" ]] && \
        source "$_filename" || \
            _ocdctrl_completion_log "Skip 2: $_filename"

    $(complete -p ocdctrl-${_cmd} 2>/dev/null) && return 0
    _ocdctrl_completion_log "Completion for [$_cmd] needed (3)"

    #
    # Try /usr/share
    _filename="/usr/share/ocdctrl/completion/ocdctrl-${_cmd}-completion"
    [[ -f "$_filename" ]] && \
        source "$_filename" || \
            _ocdctrl_completion_log "Skip 3: $_filename"

    $(complete -p ocdctrl-${_cmd} 2>/dev/null) && return 0
    _ocdctrl_completion_log "Completion for [$_cmd] needed (3)"


    return 1
}

function _ocdctrl_completion()
{
    function log_arrays() {
        _ocdctrl_completion_log "$(declare -p COMP_WORDS)"
    }
    _ocdctrl_completion_log

    if [ ${#OCDCTRL_SUBCMD[@]} -eq 0 ]; then
        local -i _j

        _ocdctrl_completion_log "Creating 1:st level sub-commands dictionary"
        readarray -t OCDCTRL_SUBCMD <<< $(ocdctrl help -l | tr '[:space:]' '\n')

        for _j in ${!OCDCTRL_SUBCMD[@]}; do
            OCDCTRL_SUBCMD_IDX[${OCDCTRL_SUBCMD[$_j]}]=$_j
        done
        _ocdctrl_completion_log $(declare -p OCDCTRL_SUBCMD)
        _ocdctrl_completion_log $(declare -p OCDCTRL_SUBCMD_IDX)

        _ocdctrl_completion_log "Creating 1:st level options dictionary"
        readarray -t OCDCTRL_OPTS <<< $(_ocdctrl_completion_optarg_for ocdctrl)

        local _opt
        local _optarg
        for _j in ${!OCDCTRL_OPTS[@]}; do
            _opt=$(awk '{print $1}' <<< ${OCDCTRL_OPTS[$_j]})
            _optarg=$(awk '{print $2}' <<< ${OCDCTRL_OPTS[$_j]})

            # Adjust opts-array
            OCDCTRL_OPTS[$_j]=${_opt}

            OCDCTRL_OPTS_IDX[${_opt}]=$_j
            if [ "X$_optarg" != "X" ]; then
                OCDCTRL_OPTS_ARG[${_opt}]=$_optarg
            fi
        done
        _ocdctrl_completion_log $(declare -p OCDCTRL_OPTS)
        _ocdctrl_completion_log $(declare -p OCDCTRL_OPTS_IDX)
        _ocdctrl_completion_log $(declare -p OCDCTRL_OPTS_ARG)
    fi

    COMPREPLY=()
    local cur=${COMP_WORDS[COMP_CWORD]}
    local prev=${COMP_WORDS[$((COMP_CWORD - 1))]}
    local _lastcmd="$(_ocdctrl_completion_last_cmd)"
    local -i _cmd_depth=$(_ocdctrl_completion_cmd_depth)

    _ocdctrl_completion_log "Last-cmd/depth: ${_lastcmd}/${_cmd_depth}"
    if [ $_cmd_depth -gt 1 ]; then
        if [ "${OCDCTRL_SUBCMD_IDX[$_lastcmd]}" ]; then
            _ocdctrl_completion_loader $_lastcmd || return 1
            _ocdctrl_completion_log "Jumps to next level: $_lastcmd"
            _ocdctrl_${_lastcmd}_completion
        else
            _ocdctrl_completion_log "Error: bad sub-cmd: $_lastcmd"
        fi
    else
        local _cmds="${OCDCTRL_SUBCMD[@]}"
        local _opts="${OCDCTRL_OPTS[@]}"
        case "$cur" in
            -*)
                _ocdctrl_completion_log "Primary opts: ${_opts}"
                COMPREPLY=($(compgen -W "${_opts}" -- "$cur"))
                ;;
            *)
                _ocdctrl_completion_log "Primary cmds: ${_cmds}"
                COMPREPLY=($(compgen -W "${_cmds}" -- "$cur"))
                ;;
        esac
    fi

    log_arrays
}

# Register for all aliases
for A in $(alias | grep ocdctrl | cut -f1 -d"=" | awk '{print $2}'); do
    complete -F _ocdctrl_completion $A
done

complete -F _ocdctrl_completion ocdctrl
complete -F _ocdctrl_completion ocdctrl.sh
complete -F _ocdctrl_completion __ocdctrl
