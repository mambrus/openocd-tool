#!/bin/bash

declare -a -g OCDCTRL_CMD_SUBCMD=()
declare -A -g OCDCTRL_CMD_SUBCMD_IDX=()
declare -A -g OCDCTRL_CMD_SUBCMD_ARG=()
declare -a -g OCDCTRL_CMD_OPTS=()
declare -A -g OCDCTRL_CMD_OPTS_IDX=()
declare -A -g OCDCTRL_CMD_OPTS_ARG=()


function _ocdctrl_cmd_completion()
{
    _ocdctrl_completion_log
    _ocdctrl_completion_log "== cmd =="

    COMPREPLY=()
    local cur=${COMP_WORDS[COMP_CWORD]}
    local prev=${COMP_WORDS[$((COMP_CWORD - 1))]}
    local _lastcmd="$(_ocdctrl_completion_last_cmd)"
    local _lastarg=$(_ocdctrl_completion_last_nonopt)

    local _cmds="${OCDCTRL_CMD_SUBCMD[@]}"
    local _opts="${OCDCTRL_CMD_OPTS[@]}"
    local _ccmd=$(_ocdctrl_completion_first_from_after cmd "${OCDCTRL_CMD_SUBCMD[@]}")
    local -a _thus_far=()
    local _suggests

    if [ ${#OCDCTRL_CMD_SUBCMD[@]} -eq 0 ]; then
        local -i _j

        _ocdctrl_completion_log "Creating sub-level sub-commands dictionary"
        readarray -t OCDCTRL_CMD_SUBCMD <<< $(ocdctrl cmd -l | \
            awk  '/^[[:alnum:]]/{print}'
        )

        for _j in ${!OCDCTRL_CMD_SUBCMD[@]}; do
            OCDCTRL_CMD_SUBCMD_IDX[${OCDCTRL_CMD_SUBCMD[$_j]}]=$_j
        done
        _ocdctrl_completion_log $(declare -p OCDCTRL_CMD_SUBCMD)
        _ocdctrl_completion_log $(declare -p OCDCTRL_CMD_SUBCMD_IDX)

        _ocdctrl_completion_log "Creating sub-level options dictionary"
        readarray -t OCDCTRL_CMD_OPTS <<< $(_ocdctrl_completion_optarg_for cmd)

        local _opt
        local _optarg
        for _j in ${!OCDCTRL_CMD_OPTS[@]}; do
            _opt=$(awk '{print $1}' <<< ${OCDCTRL_CMD_OPTS[$_j]})
            _optarg=$(awk '{print $2}' <<< ${OCDCTRL_CMD_OPTS[$_j]})

            # Adjust opts-array
            OCDCTRL_CMD_OPTS[$_j]=${_opt}

            OCDCTRL_CMD_OPTS_IDX[${_opt}]=$_j
            if [ "X$_optarg" != "X" ]; then
                OCDCTRL_CMD_OPTS_ARG[${_opt}]=$_optarg
            fi
        done
        _ocdctrl_completion_log $(declare -p OCDCTRL_CMD_OPTS)
        _ocdctrl_completion_log $(declare -p OCDCTRL_CMD_OPTS_IDX)
        _ocdctrl_completion_log $(declare -p OCDCTRL_CMD_OPTS_ARG)
    fi

    #
    # Make sure there's API description where it's expected
    #
    function assure_desc() {
        local _cmd=$1

        if [ "${OCDCTRL_CMD_SUBCMD_ARG[$_cmd]}" ]; then
            _ocdctrl_completion_log "Using cashed lines for [$_cmd]:"
            #comp_log "Cashed lines for [$_cmd]:" \
            #   "_lines" "${OCDCTRL_CMD_SUBCMD_ARG[$_cmd]}"
        else
            local _lines="$(
                ocdctrl cmd -u $_cmd | \
                sed -e 's/^.*'$_cmd'[[:space:]]//'
            )"
            OCDCTRL_CMD_SUBCMD_ARG[$_cmd]="${_lines}"
            _ocdctrl_completion_log "Cashing for [$_cmd]" \
                " lines: $(wc -l <<< $_lines)"
        fi
    }
    #
    # For target command, reply with suitable suggestions
    # Arg 1: Command
    function command_suggest() {
        local _cmd=$1
        local _desc
        local _desc_line
        local -a _args_in_line
        local _finals

        #
        # Simplest case: [run|halt|init]
        #
        # If any element is a key-word, expand it
        #
        function unpack_simple_multi_choice() {
            #
            # Make a map with same keys as contents
            #
            local -A _map=$(echo "$1" | \
                sed -Ee 's/[<([]//g' -e 's/[])<]//g' -e 's/\|/ /g' | \
                awk '
                    BEGIN {
                        RS = "[[:space:]]";
                        FS = "\n";
                        printf("( ");
                    }
                    {
                        if ( $1 != "" )
                            printf("[%s]=%s ", $1, $1);
                    }
                    END {
                        printf(")\n");
                    }'
            )
            #
            # Look in map for key-words as keys and replace element
            #
            test "${_map[command_name]}" && _map[command_name]="${OCDCTRL_CMD_SUBCMD[@]}"

            #
            # Dump map with transformed elements
            #
            echo "${_map[@]}"
        }
        #
        # Get n'th word selected description where selection is based on list
        #
        # Args
        #  1: _idx          - i.e. "column" from _description, usually
        #                     length of "thus far" + 1
        #  2: _dscr         - Multi-lined Description to match against
        #  3: _list         - list (array) of words used thus far
        #
        function ntw_of_descr() {
            local -i _idx=$1
            shift
            local _dscr="$1"
            shift
            local -a _list=("$@")
            local -i _nlist=${#_list[@]}
            local -i _i

            function _grep() {
                local -i _depth=$(( $1 + 1 ))

                _ocdctrl_completion_log "\${_list[$_depth]}: ${_list[$_depth]}"
                if [ $_depth -lt $_nlist ]; then
                    grep "${_list[$_depth]}" | \
                    _ocdctrl_completion_plog "G[$_idx:$_depth:${_list[$_depth]}]" | \
                    awk -v depth=$_depth -v name=${_list[$_depth]} '
                        BEGIN {
                            nth=depth + 1;
                            pre_patt="[|[]*"
                            pst_patt="[|\\]]*"
                        }
                        {
                            if ( match($nth, pre_patt name pst_patt) )
                                print $0
                        }' | \
                    _ocdctrl_completion_plog "W[$_idx:$_depth:${_list[$_depth]}]" | \
                    _grep $_depth
                else
                    cat --
                fi
            }

            _ocdctrl_completion_log "~-~-~-~-~-~-~-~"
            _ocdctrl_completion_log "Filter containing:"
            _ocdctrl_completion_log "$(declare -p _list)"
            _ocdctrl_completion_log "Select nth arg [$_idx] from:"
            _ocdctrl_completion_log "~-~-~-~-~-~-~-~"
            _ocdctrl_completion_log "$_dscr"
            _ocdctrl_completion_log "~-~-~-~-~-~-~-~"

            echo "$_desc" | \
                _ocdctrl_completion_plog "G[$_idx:-1:]" | \
                _grep -1 | \
                awk '{print $'$_idx'}'
        }
        #
        # Helpers initialization
        #
        {
            assure_desc $_cmd
            _desc="${OCDCTRL_CMD_SUBCMD_ARG[$_cmd]}"

            # BIG FAT NOTE; The following is not a local variable. It
            # changes higher scope.
            _thus_far=($(_ocdctrl_completion_thus_far cmd "${OCDCTRL_CMD_SUBCMD[@]}"))

            _ocdctrl_completion_log "Parse for [$_cmd] from thus-far:" \
                "$(declare -p _thus_far)"

        }
        #
        # Main logic
        #
        _ocdctrl_completion_log "Cross suggest n'th [lines][arg]: " \
            "[$(wc -l <<< $_desc)][${#_thus_far[@]}]"

        # Out of several (possible) description lines, select the ones from
        # where choices has already been made so far
        _desc_line=$(ntw_of_descr \
            $(( ${#_thus_far[@]} + 1 )) \
            "$_desc" \
            "${_thus_far[@]}")

        # Unpack a multi-choice
        if  [[ "$_desc_line" == *"|"* ]]; then
            readarray -t -d' ' _args_in_line <<< "${_desc_line}"
            # Multi-choice's like: [run|halt|init]
            _ocdctrl_completion_log "Simple multi-choice[${#_args_in_line[@]}]" \
                "suggest thus far [${#_thus_far[@]}] choose from: ${_desc_line}"

            _desc_line="$(unpack_simple_multi_choice $_desc_line)"
        fi

        # Remove any final decorators (groupers)
        sed -Ee 's/[])><([]//g' <<< "$_desc_line"
        #echo "$_desc_line"
    }
    #
    # Compressed log with prefix decorator
    #
    # Args
    # 1:        prefix
    # 2:        var-name
    # 3-n:      content
    #
    function comp_log() {
        local _deco="$1"
        shift
        local _var="$1"
        shift
        local -a ____
        local _logline

        readarray -t ____ <<< "${@}"
        _logline="$(declare -p ____)"
        _logline="$(sed -e 's/____/'$_var'/' <<< $_logline)"

        _ocdctrl_completion_log "$_deco: $_logline"
    }

    _ocdctrl_completion_log "Last-cmd/arg/cur: ${_lastcmd}/${_lastarg}/${cur}"

    # This level selection is still pending if cur has content
    if [ "X$cur" == "X" ]; then
        if [ "X$_ccmd" == "X" ] && ! [ "${OCDCTRL_CMD_SUBCMD_IDX[$_lastarg]}" ]; then
            _ocdctrl_completion_log "End-level reached, available cmds: ${_cmds}"
            COMPREPLY=($(compgen -W "${_cmds}" -- "$cur"))
        else
            if [ "X$_ccmd" == "X" ]; then
                assure_desc $_lastarg
                _suggests=$(command_suggest $_lastarg)
                comp_log "Suggest (0): " "_suggests" "$_suggests"
                COMPREPLY=($(compgen -W "$_suggests" -- "$cur"))
            else
                assure_desc $_ccmd
                _suggests="$(command_suggest $_ccmd)"
                comp_log "Suggest (2): " "_suggests" "$_suggests"
                COMPREPLY=($(compgen -W "$_suggests" -- "$cur"))
            fi
        fi
    else
        case "$cur" in
            -*)
                _ocdctrl_completion_log "Sub-opts: ${_opts}"
                COMPREPLY=($(compgen -W "${_opts}" -- "$cur"))
                ;;
            *)
                if [ "X$_ccmd" == "X" ]; then
                    _ocdctrl_completion_log "Sub-cmds: ${_cmds}"
                    COMPREPLY=($(compgen -W "${_cmds}" -- "$cur"))
                else
                    _thus_far=($(_ocdctrl_completion_thus_far cmd "${OCDCTRL_CMD_SUBCMD[@]}"))
                    _suggests="$(command_suggest $_ccmd)"
                    comp_log "Suggest (1): " "_suggests" "$_suggests"
                    COMPREPLY=($(compgen -W "$_suggests" -- "$cur"))
                fi
                ;;
        esac
    fi

}

complete -F _ocdctrl_cmd_completion ocdctrl-cmd
complete -F _ocdctrl_cmd_completion ocdctrl-cmd.sh
complete -F _ocdctrl_cmd_completion __ocdctrl_cmd

# Register for all aliases too
for A in $(alias | grep ocdctrl-cmd | cut -f1 -d"=" | awk '{print $2}'); do
    complete -F _ocdctrl_cmd_completion $A
done

