# Shell auto-completion

Auto-completion, also called tab-expansion, helps using TUI based
utilities. Henceforth auto-completion will be referred to as AC.

This project utilizes *dynamic* AC by querying the application's
introspection help-system which in turn if it doesn't know the answer itself
will query the back-end, i.e. OpenODC.

References: 
* OpenOCD project [main-site](http://openocd.org/)
* OpenOCD [git-repository](https://sourceforge.net/projects/openocd/)
* Latest [OpenODC User's
  Guide](http://openocd.org/doc-release/pdf/openocd.pdf)
* TLDP's [An Introduction to Programmable Completion
  ](https://tldp.org/LDP/abs/html/tabexpansion.html) (Bash)

## The benefit

* Much less maintenance burden as AC's for projects are usually projects of
  their own in parallel of the application project
  * Hence they don't keep up or are more crippled or limited than necessry
* AC will adapt to OpenOCD changes
  * New or deference in commands
  * Automatically adapt to differences existence/absence of particular
    target/MCU commands depending on how OpenOCD was started.
* OpenOCD is huge. It's lack of AC-based interactivity does not promote
  exploration of it's many features. This helps with that. 

## The drawback

* Would be slower if naively done
* Logic of the code is more hairy than a direct approach as replies to the
  shell are analysed and interpreted in run-time
  * Which is not strictly a draw-back as OpenOCD command semantics follow
    uniform rules.
  * It enforces strictness and rules in the project it serves, which isn't
    really a draw-back either.
* AC's can often become huge. Reader is encouraged to glance on the
  [git-command's
  ditto](https://github.com/git/git/blob/master/contrib/completion/git-completion.bash)

## Design

To overcome the drawback of beings slower due to query and analyse, both the
query, result and parsed expectations are cashed in associative arrays which
are very fast. I.e. penalty occurs only once. Since AC's are bash-shell
process extensions, they live only as long as the process and they don't
affect each other. I.e. other OpenOCD instances supporting other commands
under other shell-processes will work happily side-by-side even if they
differ.

`ocdctrl` has lent some of it's structural design from the git-command, in
that it's easily expendable by just adding sub-commands in a directory. The
AC for each sub-command follows the same paradigm. One AC for one
sub-command. As `ocdctrl` handles is sub-commands as sourced functions AC
needs to take into account if it's managing from the root-command
executable, the sub-command library executable or it's corresponding
function. This might seem as an obstacle but is the reason why the complete
AC becomes modular. As each AC fallow the same set of rules, the become easy
to maintain and extend as well.





