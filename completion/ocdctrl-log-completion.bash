#!/bin/bash

declare -a -g OCDCTRL_LOG_SUBCMD=()
declare -A -g OCDCTRL_LOG_SUBCMD_IDX=()
declare -a -g OCDCTRL_LOG_OPTS=()
declare -A -g OCDCTRL_LOG_OPTS_IDX=()
declare -A -g OCDCTRL_LOG_OPTS_ARG=()


function _ocdctrl_log_completion()
{
    _ocdctrl_completion_log
    _ocdctrl_completion_log "== log =="

    COMPREPLY=()
    local cur=${COMP_WORDS[COMP_CWORD]}
    local prev=${COMP_WORDS[$((COMP_CWORD - 1))]}
    local _lastcmd="$(_ocdctrl_completion_last_cmd)"
    local _lastarg=$(_ocdctrl_completion_last_nonopt)

    if [ ${#OCDCTRL_LOG_SUBCMD[@]} -eq 0 ]; then
        local -i _j

        _ocdctrl_completion_log "Creating sub-level sub-commands dictionary"
        readarray -t OCDCTRL_LOG_SUBCMD <<< $(ocdctrl log -h | \
            sed -n '/^ARGUMENTS/,/^SEE ALSO/P' | \
            grep -Ee '[[:space:]]-[[:space:]]' | \
            awk '{print $1}' | tr '[:space:]' '\n'
        )

        for _j in ${!OCDCTRL_LOG_SUBCMD[@]}; do
            OCDCTRL_LOG_SUBCMD_IDX[${OCDCTRL_LOG_SUBCMD[$_j]}]=$_j
        done
        _ocdctrl_completion_log $(declare -p OCDCTRL_LOG_SUBCMD)
        _ocdctrl_completion_log $(declare -p OCDCTRL_LOG_SUBCMD_IDX)

        _ocdctrl_completion_log "Creating sub-level options dictionary"
        readarray -t OCDCTRL_LOG_OPTS <<< $(_ocdctrl_completion_optarg_for log)

        local _opt
        local _optarg
        for _j in ${!OCDCTRL_LOG_OPTS[@]}; do
            _opt=$(awk '{print $1}' <<< ${OCDCTRL_LOG_OPTS[$_j]})
            _optarg=$(awk '{print $2}' <<< ${OCDCTRL_LOG_OPTS[$_j]})

            # Adjust opts-array
            OCDCTRL_LOG_OPTS[$_j]=${_opt}

            OCDCTRL_LOG_OPTS_IDX[${_opt}]=$_j
            if [ "X$_optarg" != "X" ]; then
                OCDCTRL_LOG_OPTS_ARG[${_opt}]=$_optarg
            fi
        done
        _ocdctrl_completion_log $(declare -p OCDCTRL_LOG_OPTS)
        _ocdctrl_completion_log $(declare -p OCDCTRL_LOG_OPTS_IDX)
        _ocdctrl_completion_log $(declare -p OCDCTRL_LOG_OPTS_ARG)
    fi

    _ocdctrl_completion_log "Last-cmd/arg/cur: ${_lastcmd}/${_lastarg}/${cur}"
    local _cmds="${OCDCTRL_LOG_SUBCMD[@]}"
    local _opts="${OCDCTRL_LOG_OPTS[@]}"
    # This level selection is still pending if cur has content
    if [ "X$cur" == "X" ]; then
        if ! [ "${OCDCTRL_LOG_SUBCMD_IDX[$_lastarg]}" ]; then
            _ocdctrl_completion_log "End-level reached, available cmds: ${_cmds}"
            COMPREPLY=($(compgen -W "${_cmds}" -- "$cur"))
        else
            _ocdctrl_completion_log "End-level reached and complete"
            COMPREPLY=()
        fi
    else
        case "$cur" in
            -*)
                _ocdctrl_completion_log "Sub-opts: ${_opts}"
                COMPREPLY=($(compgen -W "${_opts}" -- "$cur"))
                ;;
            *)
                _ocdctrl_completion_log "Sub-cmds: ${_cmds}"
                COMPREPLY=($(compgen -W "${_cmds}" -- "$cur"))
                ;;
        esac
    fi

}

complete -F _ocdctrl_log_completion ocdctrl-log
complete -F _ocdctrl_log_completion ocdctrl-log.sh
complete -F _ocdctrl_log_completion __ocdctrl_log

# Register for all aliases too
for A in $(alias | grep ocdctrl-log | cut -f1 -d"=" | awk '{print $2}'); do
    complete -F _ocdctrl_log_completion $A
done

