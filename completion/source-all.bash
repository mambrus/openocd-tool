#! /bin/bash

#
# Figure out which base-directory
#
function deduct_base() {
    local _base

    # Figure out base-dir
    # Try dev first
    _base="$(dirname $(which ocdctrl.sh))"

    if [ "X$_base" == "X" ]; then
        _base="$(dirname $(which ocdctrl))"
    fi

    if [[ $_base =~ ^/usr/local/ ]]; then
        _base="/usr/local/share/bash-completion/completions/ocdctrl"
    else if [[ $_base =~ ^/usr/ ]]; then
        _base="/usr/share/bash-completion/completions/ocdctrl"
    else
        _base=$(cd $_base/../completion; pwd)
    fi fi

    echo $_base
}
#
# Source files from appropriate directory
#
# Note: Sourced with full pathname for security reasons (PATH)
#
function source_files() {
    local _base

    _base="$(deduct_base)"
    if [ "X$_base" == "X" ]; then
        echo "ocdctr not found. No files can be sourced" >&2
    else
        local _f
        echo "Sourcing from: $_base" >&2
        echo "" >&2

        for _f in $(find $_base -name "ocdctrl-*completion.bash"); do
            echo "sourcing: $_f" >&2
            source "$_f"
        done
    fi
    echo "" >&2
}

if ! (return 0 2>/dev/null); then
    echo "Error: You need to source this file!" >&2
    exit 1
fi

source_files
more_to_source="$(deduct_base)/export-functions.bash"
if [ -f "$more_to_source" ]; then
    source "$more_to_source"
else
    echo "File not found. Can't export functions" >&2
fi

unset more_to_source
