# README ocdctrl

`ocdctrl` is a console-application tool managing `OpenOCD`

For more information, read the
[Wiki](https://gitlab.com/mambrus/openocd-tool/-/wikis/home)

## Release history

* ocdctrl-0.0.1 21'th March 2023

First release

